<?php

namespace App\Http\Livewire\Ciclo;

use App\Models\Ciclo;
use Livewire\Component;
use Livewire\WithPagination;

class CiclosIndex extends Component
{
    public function render()
    {
        $ciclosSecciones = Ciclo::select('seccion')->groupByRaw('seccion')->get();
        return view('livewire.ciclo.ciclos-index', compact('ciclosSecciones'));
    }
}
