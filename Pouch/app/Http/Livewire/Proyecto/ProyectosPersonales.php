<?php

namespace App\Http\Livewire\Proyecto;

use App\Models\Proyecto;
use Livewire\Component;
use Livewire\WithPagination;


class ProyectosPersonales extends Component
{

    use WithPagination;
    public $search;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $proyectos = Proyecto::latest('created_at')
            ->where('user_id',auth()->user()->id)
            ->get();
        return view('livewire.proyecto.proyectos-personales', compact('proyectos'));
    }
}
