<?php

namespace App\Http\Livewire\Proyecto;

use App\Models\CalificacionDet;
use App\Models\Proyecto;
use App\Models\RubricaAplicacionCab;
use App\Models\RubricaAplicacionDet;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ProyectosEvaluarAplicacion extends Component
{
    public $proyecto;
    public $notas = [];
    public $relleno = false;
    public $finalizado;

    public function render()
    {
        $this->cargarDatos();
        if (in_array(null, $this->notas)) {
            $this->relleno = false;
        } else {
            $this->relleno = true;
        }
        $this->finalizado = DB::table('calificaciones_proyecto')
            ->where('user_id', auth()->user()->id)
            ->where('proyecto_id', $this->proyecto->id)
            ->where('calificacion_cab_id', 3)
            ->value('finalizado');
        $rubricasCab = RubricaAplicacionCab::all();
        $proyecto = $this->proyecto;
        $notas = $this->notas;
        return view('livewire.proyecto.proyectos-evaluar-aplicacion', compact('rubricasCab', 'proyecto', 'notas'));
    }

    public function cargarDatos()
    {
        $puntuaciones = CalificacionDet::where('proyecto_id', $this->proyecto->id)
            ->where('user_id', auth()->user()->id)
            ->where('calificacion_cab_id', 3)
            ->orderBy('rubrica_det_id', 'asc')->get();
        for ($i = 1; $i <= RubricaAplicacionDet::all()->count(); $i++) {
            $this->notas[$i] = null;
        }
        foreach ($puntuaciones as $puntuacion) {
            $this->notas[$puntuacion->rubrica_det_id] = $puntuacion->puntuacion;
        }
    }

    public function finalizarPuntuacion()
    {
        try {
            if ($this->relleno) {
                $puntuacion = 0;
                $rubricasCab = RubricaAplicacionCab::all();
                foreach ($rubricasCab as $rubricaCab) {
                    $porcentaje = $rubricaCab->porcentaje;
                    $rubricasDet = CalificacionDet::where('calificacion_cab_id', 3)
                        ->where('user_id', auth()->user()->id)
                        ->where('proyecto_id', $this->proyecto->id)
                        ->where('rubrica_cab_id', $rubricaCab->id)->get();
                    $puntuacionAux = 0;
                    foreach ($rubricasDet as $rubricaDet) {
                        $puntuacionAux += $rubricaDet->puntuacion;
                    }
                    $puntuacion += ((100 * $puntuacionAux) / (count($rubricasDet) * 5)) * ($porcentaje / 100);
                }
                DB::table('calificaciones_proyecto')->insert([
                    'calificacion_cab_id' => 3,
                    'proyecto_id' => $this->proyecto->id,
                    'user_id' => auth()->user()->id,
                    'puntuacion' => (($puntuacion * 3) / 10),
                    'finalizado' => 1,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
                $profesores = $this->proyecto->users;
                $evaluar = true;
                $puntuacionTotal = 0;
                foreach ($profesores as $profesor) {
                    if (
                        DB::table('calificaciones_proyecto')
                        ->where('user_id', $profesor->id)
                        ->where('proyecto_id', $this->proyecto->id)
                        ->count() != 4
                    ) {
                        $evaluar = false;
                    } else {
                        $puntuacionTotal += DB::table('calificaciones_proyecto')
                            ->where('user_id', $profesor->id)
                            ->where('proyecto_id', $this->proyecto->id)
                            ->sum('puntuacion');
                    }
                }
                if ($evaluar) {
                    $this->proyecto->update([
                        'puntuacion' => $puntuacionTotal / count($profesores)
                    ]);
                }
                session()->flash('mensajeAplicacionFinalizado', 'Se ha evaluado la aplicacion correctamente');
                return redirect()->route('proyecto.secciones', $this->proyecto);
            }
        } catch (\Exception $e) {
            session()->flash('mensajeError', '¡Ups! Ha ocurrido un problema. Usted no debería modificar cosas que sean suyas.');
            return redirect()->route('proyecto.secciones', $this->proyecto);
        };
    }

    public function almacenarNotas($rubricaId)
    {
        $rubricaDet = RubricaAplicacionDet::find($rubricaId);
        if (CalificacionDet::where('rubrica_det_id', $rubricaId)
            ->where('user_id', auth()->user()->id)
            ->where('proyecto_id', $this->proyecto->id)
            ->where('calificacion_cab_id', 3)
            ->exists()
        ) {
            $calificacionDet = CalificacionDet::where('rubrica_det_id', $rubricaId)
                ->where('user_id', auth()->user()->id)
                ->where('proyecto_id', $this->proyecto->id)
                ->where('calificacion_cab_id', 3)
                ->first();
            $calificacionDet->update([
                'proyecto_id' => $this->proyecto->id,
                'calificacion_cab_id' => 3,
                'rubrica_cab_id' => $rubricaDet->rubrica_aplicacion_cab_id,
                'rubrica_det_id' => $rubricaId,
                'puntuacion' =>  $this->notas[$rubricaId],
            ]);
        } else {
            CalificacionDet::create([
                'user_id' => auth()->user()->id,
                'proyecto_id' => $this->proyecto->id,
                'calificacion_cab_id' => 3,
                'rubrica_cab_id' => $rubricaDet->rubrica_aplicacion_cab_id,
                'rubrica_det_id' => $rubricaId,
                'puntuacion' =>  $this->notas[$rubricaId],
            ]);
        }
        $this->emit('closeModal', $rubricaId);
    }
}
