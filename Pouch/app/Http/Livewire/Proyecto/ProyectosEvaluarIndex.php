<?php

namespace App\Http\Livewire\Proyecto;

use Livewire\Component;

class ProyectosEvaluarIndex extends Component
{
    public function render()
    {
        $proyectos = auth()->user()->proyectosAsociados;
        return view('livewire.proyecto.proyectos-evaluar-index',compact('proyectos'));
    }
}
