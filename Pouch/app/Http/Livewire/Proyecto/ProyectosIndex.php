<?php

namespace App\Http\Livewire\Proyecto;

use App\Models\Proyecto;
use Livewire\Component;

class ProyectosIndex extends Component
{

    public function render()
    {
        $proyectos = Proyecto::orderByDesc('created_at')->get();
        return view('livewire.proyecto.proyectos-index', compact('proyectos'));
    }
}
