<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProyectoRequest;
use App\Models\CalificacionDet;
use App\Models\Ciclo;
use App\Models\Comentario;
use App\Models\Proyecto;
use App\Models\Recurso;
use App\Models\RubricaAplicacionDet;
use App\Models\RubricaDocumentacionCab;
use App\Models\RubricaDocumentacionDet;
use App\Models\RubricaExposicionDet;
use App\Models\RubricaSeguimientoDet;
use App\Models\TipoRecurso;
use App\Models\User;
use App\Traits\ControlUsersTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ZipArchive;
use File;
use Illuminate\Support\Facades\DB;

class ProyectoController extends Controller
{
    use ControlUsersTrait;

    public function __construct()
    {
        //$this->middleware('can:proyectos.evaluar.index');
    }

    public function index()
    {
        return view('proyectos.index');
    }

    public function create()
    {
        $ciclos = Ciclo::all();
        return view('proyectos.create', compact('ciclos'));
    }

    public function store(ProyectoRequest $request)
    {
        $publico = 1;
        if (!$request->has('publico')) {
            $publico = 0;
        }

        $proyecto = new Proyecto();
        $proyecto->fill($request->all());
        $proyecto->publico = $publico;
        $proyecto->created_at = Carbon::now()->toDateTimeString();
        $proyecto->updated_at =Carbon::now()->toDateTimeString();
        $proyecto->save();

        $this->registrarControlUsers($proyecto->id, "PROYECTO CREADO");

        if ($request->file('ruta_logo')) {
            $nombreArchivo = $request->file('ruta_logo')->getClientOriginalName();
            $proyecto->update([
                'ruta_logo' => Storage::putFileAs('recursos/' . $proyecto->id, $request->file('ruta_logo'), $nombreArchivo)
            ]);
            $this->registrarControlUsers($proyecto->id, "LOGO SUBIDO");
        } else {
            $proyecto->update([
                'ruta_logo' => Storage::putFileAs('recursos/' . $proyecto->id, 'images/defaultwhite.jpg', 'defaultwhite.jpg')
            ]);
        }

        if ($request->file('ruta_fichero')) {
            $nombreArchivo = $request->file('ruta_fichero')->getClientOriginalName();
            $urlFile = Storage::putFileAs('recursos/' . $proyecto->id, $request->file('ruta_fichero'), $nombreArchivo);
            $extension = pathinfo($urlFile, PATHINFO_EXTENSION);

            $proyecto->recursos()->create([
                'ruta_fichero' => $urlFile,
                'tipo_id' => TipoRecurso::where('descripcion', 'LIKE', '%' . $extension . '%')->first()->id
            ]);
            $this->registrarControlUsers($proyecto->id, "CÓDIGO DEL PROYECTO SUBIDO");
        }

        if ($request->file('ruta_documentacion')) {
            $nombreArchivo = $request->file('ruta_documentacion')->getClientOriginalName();
            $urlFile = Storage::putFileAs('recursos/' . $proyecto->id, $request->file('ruta_documentacion'), $nombreArchivo);
            $extension = pathinfo($urlFile, PATHINFO_EXTENSION);
            $proyecto->recursos()->create([
                'ruta_fichero' => $urlFile,
                'tipo_id' => TipoRecurso::where('descripcion', 'LIKE', '%' . $extension . '%')->first()->id
            ]);
            $this->registrarControlUsers($proyecto->id, "DOCUMENTACIÓN DEL PROYECTO SUBIDO");
        }
        return redirect()->route('proyectos.show', $proyecto)->with('createInfo', 'Proyecto creado');
    }

    public function show(Proyecto $proyecto)
    {
        $registros = $proyecto->user->registros->where('proyecto_id', $proyecto->id); 
        

        // LOGO
        $ultimosRegistros[] = $registros->sortByDesc('created_at')->first(function ($item){
            return false !== stristr($item->accion, 'LOGO');
        });

        //CÓDIGO
        $ultimosRegistros[]  = $registros->sortByDesc('created_at')->first(function ($item){
            return false !== stristr($item->accion, 'CÓDIGO');
        });

        //DOCUMENTACIÓN
        $ultimosRegistros[]  = $registros->sortByDesc('created_at')->first(function ($item){
            return false !== stristr($item->accion, 'DOCUMENTACIÓN');
        });
        $ultimosRegistros[] = $registros->last();
        $ultimosRegistros = array_filter($ultimosRegistros, fn($value) => !is_null($value) && $value !== '');
        $ultimosRegistros = collect($ultimosRegistros)->sortByDesc('created_at');
        return view('proyectos.show', compact('proyecto','ultimosRegistros'));
    }

    public function edit(Proyecto $proyecto)
    {
        $this->authorize('author', $proyecto);
        $ciclos = Ciclo::all();
        return view('proyectos.edit', compact('proyecto', 'ciclos'));
    }

    public function update(ProyectoRequest $request, Proyecto $proyecto)
    {
        $this->authorize('author', $proyecto);

        $publico = 1;
        if (!$request->has('publico')) {
            $publico = 0;
        }

        $url_logo_delete = $proyecto->ruta_logo;
        $proyecto->fill($request->all());
        $proyecto->publico = $publico;

        if ($request->file('ruta_logo')) {
            Storage::delete(($url_logo_delete));
            $nombreArchivo = $request->file('ruta_logo')->getClientOriginalName();
            $proyecto->update([
                'ruta_logo' => Storage::putFileAs('recursos/' . $proyecto->id, $request->file('ruta_logo'), $nombreArchivo)
            ]);
            $this->registrarControlUsers($proyecto->id, "LOGO MODIFICADO");
        }

        $proyecto->save();

        if ($request->file('ruta_fichero')) {

            $nombreArchivo = $request->file('ruta_fichero')->getClientOriginalName();
            $urlFile = Storage::putFileAs('recursos/' . $proyecto->id, $request->file('ruta_fichero'), $nombreArchivo);
            $extension = pathinfo($urlFile, PATHINFO_EXTENSION);
            $recurso = $proyecto->recursos->whereIn('tipo_id', [1, 2])->first();

            if ($recurso) {
                Storage::delete($recurso->ruta_fichero);
                $recurso->update([
                    'ruta_fichero' => $urlFile,
                    'tipo_id' => TipoRecurso::where('descripcion', 'LIKE', '%' . $extension . '%')->first()->id
                ]);
                $this->registrarControlUsers($proyecto->id, "CÓDIGO DEL PROYECTO MODIFICADO");
            } else {
                $proyecto->recursos()->create([
                    'ruta_fichero' => $urlFile,
                    'tipo_id' => TipoRecurso::where('descripcion', 'LIKE', '%' . $extension . '%')->first()->id
                ]);
                $this->registrarControlUsers($proyecto->id, "CÓDIGO DEL PROYECTO SUBIDO");
            }
        }

        if ($request->file('ruta_documentacion')) {
            $nombreArchivo = $request->file('ruta_documentacion')->getClientOriginalName();
            $urlFile = Storage::putFileAs('recursos/' . $proyecto->id, $request->file('ruta_documentacion'), $nombreArchivo);
            $extension = pathinfo($urlFile, PATHINFO_EXTENSION);
            $recurso = $proyecto->recursos->whereIn('tipo_id', [3, 4])->first();

            if ($recurso) {
                Storage::delete($recurso->ruta_fichero);
                $recurso->update([
                    'ruta_fichero' => $urlFile,
                    'tipo_id' => TipoRecurso::where('descripcion', 'LIKE', '%' . $extension . '%')->first()->id
                ]);
                $this->registrarControlUsers($proyecto->id, "DOCUMENTACIÓN DEL PROYECTO MODIFICADO");
            } else {
                $proyecto->recursos()->create([
                    'ruta_fichero' => $urlFile,
                    'tipo_id' => TipoRecurso::where('descripcion', 'LIKE', '%' . $extension . '%')->first()->id
                ]);
                $this->registrarControlUsers($proyecto->id, "DOCUMENTACIÓN DEL PROYECTO SUBIDO");
            }
        }

        return redirect()->route('proyectos.edit', $proyecto)->with('updateInfo', 'Proyecto actualizado con éxito');
    }

    public function download(Proyecto $proyecto)
    {
        if ($proyecto->publico) {
            $zip = new ZipArchive;
            $fileName = $proyecto->nombre . ".rar";
            Storage::makeDirectory('proyectos/' . $proyecto->id);
            if ($zip->open(public_path('storage/proyectos/' . $proyecto->id . '/' . $fileName), ZipArchive::CREATE) === true) {
                $files = File::files(public_path('storage/recursos/' . $proyecto->id));
                foreach ($files as $key => $value) {
                    $relativeNameInZipFile = basename($value);
                    $zip->addFile($value, $relativeNameInZipFile);
                }
                $zip->close();
            }
            $proyecto->update([
                'descargas' => $proyecto->descargas + 1
            ]);
            return response()->download(public_path('storage/proyectos/' . $proyecto->id . '/' . $fileName))->deleteFileAfterSend(true);
        }
        return redirect()->route('inicio');
    }

    public function personales()
    {
        $proyectos = Proyecto::all();
        return view('proyectos.personales', compact('proyectos'));
    }

    public function destroy(Proyecto $proyecto)
    {
        $this->authorize('author', $proyecto);
        $proyecto->delete();
        Storage::deleteDirectory('proyectos/' . $proyecto->id);
        Storage::deleteDirectory('recursos/' . $proyecto->id);
        return redirect()->route('proyectos.personales')->with('destroyInfo', 'El proyecto se ha eliminado con éxito');
    }

    public function comentar(Request $request, Proyecto $proyecto)
    {
        Comentario::create([
            'proyecto_id' => $proyecto->id,
            'user_id' => auth()->user()->id,
            'descripcion' => $request->comentario
        ]);
        return redirect()->route('proyectos.show', $proyecto)->with('comentarioInfo', 'El comentario se ha insertado con éxito');
    }

    public function eliminarComentario($id)
    {
        $comentario = Comentario::where('id', $id)->first();
        $proyecto = Proyecto::where('id', $comentario->proyecto_id)->first();
        $comentario->delete();
        return redirect()->route('proyectos.show', $proyecto)->with('eliminarComentarioInfo', 'El comentario ha sido eliminado con éxito');
    }

    public function descargarLogo(Proyecto $proyecto)
    {
        return Storage::download($proyecto->ruta_logo);
    }

    public function descargarRecurso(Proyecto $proyecto, Recurso $recurso)
    {
        return Storage::download($recurso->ruta_fichero);
    }

    public function mostrarPDF(Proyecto $proyecto)
    {
        $rutaPDF = $proyecto->recursos->where('tipo_id', '=', '3')->first()->ruta_fichero;
        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=default.pdf");
        readfile(public_path('storage/' . $rutaPDF));
    }

    public function evaluarIndex()
    {
        return view('proyectos.evaluar.index');
    }

    public function asignarUsuarios(Proyecto $proyecto)
    {
        $this->authorize('author', $proyecto);
        if (!empty($proyecto->puntuacion)) {
            $proyectos = Proyecto::all();
            return redirect()->route('proyectos.personales');
        }
        $profesores = User::role('profesor')->get();
        $usuariosAsignados = $proyecto->users;
        $profesoresDisponibles =  array();

        if (count($profesores) != 0) {
            foreach ($profesores as $profesor) {
                if (!$usuariosAsignados->contains('id', $profesor->id)) {
                    $profesoresDisponibles[] = $profesor;
                }
            }
        }
        return view('proyectos.asignar', compact('proyecto', 'usuariosAsignados', 'profesoresDisponibles'));
    }

    public function asignar(Request $request, Proyecto $proyecto)
    {
        $this->authorize('author', $proyecto);
        foreach ($request->users as $id) {
            $proyecto->users()->attach([
                $id
            ]);
        }
        return redirect()->route('proyectos.asignarUsuarios', $proyecto)->with('asociarInfo', 'Se han asociado los usuarios seleccionados con éxito.');
    }

    public function desasignar(Proyecto $proyecto, User $user)
    {
        try {
            $this->authorize('author', $proyecto);
            $proyecto->users()->detach($user->id);
            return redirect()->route('proyectos.asignarUsuarios', $proyecto)->with('desasociarInfo', 'Se ha desasignado el profesor con éxito.');
        } catch (\Throwable $th) {
            return redirect()->route('proyectos.asignarUsuarios', $proyecto)->with('desasociarInfoError', 'Ha ocurrido un error al desasignar el profesor. Intentelo de nuevo.');
        }
    }

    public function evaluarSecciones(Proyecto $proyecto)
    {
        $numeroRubricas = [];
        $numeroRubricas[1] = array(
            'rubricasCompletadas' => CalificacionDet::where('proyecto_id', $proyecto->id)->where('user_id', auth()->user()->id)->where('calificacion_cab_id', 1)->count(),
            'rubricasDisponibles' => RubricaExposicionDet::all()->count(),
            'puntuacion' => DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->where('calificacion_cab_id', 1)->value('puntuacion'),
            'finalizado' => DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->where('calificacion_cab_id', 1)->value('finalizado')
        );
        $numeroRubricas[2] = array(
            'rubricasCompletadas' => CalificacionDet::where('proyecto_id', $proyecto->id)->where('user_id', auth()->user()->id)->where('calificacion_cab_id', 2)->count(),
            'rubricasDisponibles' => RubricaDocumentacionDet::all()->count(),
            'puntuacion' => DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->where('calificacion_cab_id', 2)->value('puntuacion'),
            'finalizado' => DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->where('calificacion_cab_id', 2)->value('finalizado')
        );
        $numeroRubricas[3] = array(
            'rubricasCompletadas' => CalificacionDet::where('proyecto_id', $proyecto->id)->where('user_id', auth()->user()->id)->where('calificacion_cab_id', 3)->count(),
            'rubricasDisponibles' => RubricaAplicacionDet::all()->count(),
            'puntuacion' => DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->where('calificacion_cab_id', 3)->value('puntuacion'),
            'finalizado' => DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->where('calificacion_cab_id', 3)->value('finalizado')
        );
        $numeroRubricas[4] = array(
            'rubricasCompletadas' => CalificacionDet::where('proyecto_id', $proyecto->id)->where('user_id', auth()->user()->id)->where('calificacion_cab_id', 4)->count(),
            'rubricasDisponibles' => RubricaSeguimientoDet::all()->count(),
            'puntuacion' => DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->where('calificacion_cab_id', 4)->value('puntuacion'),
            'finalizado' => DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->where('calificacion_cab_id', 4)->value('finalizado')
        );
        return view('proyectos.evaluar.secciones', compact('proyecto', 'numeroRubricas'));
    }

    public function evaluarExposicion(Proyecto $proyecto)
    {
        return view('proyectos.evaluar.exposicion', compact('proyecto'));
    }

    public function evaluarDocumentacion(Proyecto $proyecto)
    {
        return view('proyectos.evaluar.documentacion', compact('proyecto'));
    }

    public function evaluarAplicacion(Proyecto $proyecto)
    {
        return view('proyectos.evaluar.aplicacion', compact('proyecto'));
    }

    public function evaluarSeguimiento(Proyecto $proyecto)
    {
        return view('proyectos.evaluar.seguimiento', compact('proyecto'));
    }

}
