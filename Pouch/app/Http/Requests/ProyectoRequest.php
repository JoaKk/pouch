<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProyectoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $proyecto = $this->route()->parameter('proyecto');

        $rules = [
            'nombre' => 'required',
            'slug' => 'required|unique:proyectos',
            'ciclo_id' => 'required',
            'descripcion' => 'required',
            'ruta_fichero' => 'mimes:rar,zip',
            'ruta_documentacion' => 'mimes:pdf,doc'
        ];
        // Cambiar reglas de validacion si existe el proyecto, por ejemplo al editar
        if ($proyecto) {
            $rules['slug'] = 'required|unique:proyectos,slug,' . $proyecto->id; //Permite introducir el mismo slug ya que pertenece al mismo proyecto
            $rules['ruta_fichero'] = 'mimes:rar,zip';
            $rules['ruta_documentacion'] = 'mimes:pdf,doc';
        }
        return $rules;
    }
    public function attributes()
    {
        return [
            'ciclo_id' => 'ciclo formativo',
            'slug' => 'título'
        ];
    }
    public function messages()
    {
        return [
            'slug.unique' => 'Ya existe un proyecto con este título. Pruebe con otro nombre'
        ];
    }
}
