<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RubricaDocumentacionDet extends Model
{
    protected $table = 'rubricas_documentacion_det';
    protected $fillable = ['descripcion','rubrica_documentacion_cab_id'];
    use HasFactory;
}
