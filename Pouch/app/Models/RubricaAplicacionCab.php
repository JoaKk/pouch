<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RubricaAplicacionCab extends Model
{
    protected $table = 'rubricas_aplicacion_cab';
    protected $fillable = ['descripcion'];
    use HasFactory;

    public function rubricasDet(){
        return $this->hasMany(RubricaAplicacionDet::class);
    }
}
