<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recurso extends Model
{
    protected $table = 'recursos';
    protected $fillable = ['ruta_fichero','tipo_id'];
    use HasFactory;
    
    public function tipoRecurso(){
        return $this->belongsTo(TipoRecurso::class);
    }
}
