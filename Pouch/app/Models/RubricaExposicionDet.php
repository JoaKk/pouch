<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RubricaExposicionDet extends Model
{
    protected $table = 'rubricas_exposicion_det';
    protected $fillable = ['descripcion','rubrica_exposicion_cab_id'];
    use HasFactory;
}
