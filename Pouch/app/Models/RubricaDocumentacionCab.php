<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RubricaDocumentacionCab extends Model
{
    protected $table = 'rubricas_documentacion_cab';
    protected $fillable = ['descripcion'];
    use HasFactory;

    public function rubricasDet(){
        return $this->hasMany(RubricaDocumentacionDet::class);
    }
}
