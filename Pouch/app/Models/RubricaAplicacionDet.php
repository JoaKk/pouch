<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RubricaAplicacionDet extends Model
{
    protected $table = 'rubricas_aplicacion_det';
    protected $fillable = ['descripcion','rubrica_aplicacion_cab_id'];
    use HasFactory;
}
