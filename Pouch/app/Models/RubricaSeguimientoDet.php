<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RubricaSeguimientoDet extends Model
{
    protected $table = 'rubricas_seguimiento_det';
    protected $fillable = ['descripcion','rubrica_seguimiento_cab_id'];
    use HasFactory;
}
