<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ControlUser extends Model
{
    protected $table = 'control_users';
    protected $fillable = ['user_id','proyecto_id','accion'];
    use HasFactory;
}
