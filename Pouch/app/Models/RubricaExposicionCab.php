<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RubricaExposicionCab extends Model
{
    protected $table = 'rubricas_exposicion_cab';
    protected $fillable = ['descripcion'];
    use HasFactory;

    public function rubricasDet(){
        return $this->hasMany(RubricaExposicionDet::class);
    }
}
