<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table = 'proyectos';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    use HasFactory;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function recursos()
    {
        return $this->hasMany(Recurso::class);
    }

    public function versiones()
    {
        return $this->hasMany(Version::class)->orderByDesc('created_at');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function ciclo()
    {
        return $this->belongsTo(Ciclo::class);
    }

    public function comentarios()
    {
        return $this->hasMany(Comentario::class)->orderByDesc('created_at');
    }

    public function calificaciones()
    {
        return $this->belongsToMany(CalificacionCab::class);
    }

    public function calcularTiempo($fecha)
    {
        $mensaje = "";
        $now = Carbon::now();
        $diffSeconds =  $fecha->diffInSeconds($now);
        $diffMinutes = $fecha->diffInMinutes($now);
        $diffHours = $fecha->diffInHours($now);
        $diffDays = $fecha->diffInDays($now);
        $diffMonths = $fecha->diffInMonths($now);
        $diffYears = $fecha->diffInYears($now);
        if ($diffMinutes < 1) {
            if ($diffSeconds > 1) {
                $mensaje = $diffSeconds . " segundos";
            } else {
                $mensaje = $diffSeconds . " segundo";
            }
        } elseif ($diffHours < 1) {
            if ($diffMinutes > 1) {
                $mensaje = $diffMinutes . " minutos";
            } else {
                $mensaje = $diffMinutes . " minuto";
            }
        } elseif ($diffDays < 1) {
            if ($diffHours > 1) {
                $mensaje = $diffHours . " horas";
            } else {
                $mensaje = $diffHours . " hora";
            }
        } elseif ($diffMonths < 1) {
            if ($diffDays > 1) {
                $mensaje = $diffDays . " días";
            } else {
                $mensaje = $diffDays . " día";
            }
        } elseif ($diffYears < 1) {
            if ($diffMonths > 1) {
                $mensaje = $diffMonths . " mes";
            } else {
                $mensaje = $diffMonths . " meses";
            }
        } else {
            if ($diffYears > 1) {
                $mensaje = $diffYears . " año";
            } else {
                $mensaje = $diffYears . " años";
            }
        }
        return $mensaje;
    }
}
