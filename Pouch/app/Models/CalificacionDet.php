<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalificacionDet extends Model
{
    protected $table = 'calificaciones_det';
    protected $fillable = ['user_id','proyecto_id','calificacion_cab_id','rubrica_cab_id','rubrica_det_id','puntuacion'];
    use HasFactory;
}
