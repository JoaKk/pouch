<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','slug', 'email','instituto_id','localidad','dni' ,'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }  

    public function adminlte_image(){
        if($this->profile_photo_path){
            return $this->profile_photo_path;
        }
        return 'https://ui-avatars.com/api/?name='.urlencode($this->name).'&color=7F9CF5&background=random&bold=true';
    }

    public function adminlte_desc(){
        if($this->hasRole([1,2,3])){
            return $this->roles->first()->name;// $this->roles->pluck('name') devuelve todas las columnas nombre
        }
        return "Pendiente de asignar rol"; 
    }

    public function adminlte_profile_url(){
        return route('perfil',auth()->user());
    }
    
    public function proyectos(){
        return $this->hasMany(Proyecto::class);
    }

    public function proyectosAsociados(){
        return $this->belongsToMany(Proyecto::class);
    }

    public function instituto(){
        return $this->belongsTo(Instituto::class);
    }

    public function comentarios(){
        return $this->hasMany(Comentario::class);
    }

    public function registros(){
        return $this->hasMany(ControlUser::class)->orderByDesc('id');
    }
}
