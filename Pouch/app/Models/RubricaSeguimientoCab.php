<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RubricaSeguimientoCab extends Model
{
    protected $table = 'rubricas_seguimiento_cab';
    protected $fillable = ['descripcion'];
    use HasFactory;

    public function rubricasDet(){
        return $this->hasMany(RubricaSeguimientoDet::class);
    }
}
