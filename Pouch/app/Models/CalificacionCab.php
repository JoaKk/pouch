<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalificacionCab extends Model
{
    protected $table = 'calificaciones_cab';
    protected $fillable = ['descripcion','porcentaje'];
    use HasFactory;

    public function proyectos()
    {
        return $this->belongsToMany(Proyecto::class);
    }
}
