<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidarDNI implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (gettype($value) == 'string') {
            $n = substr($value, 0, -1);
            $letra = substr($value, strlen($value) - 1, strlen($value));
            $resto = $n % 23;
            $cadena = "TRWAGMYFPDXBNJZSQVHLCKE";
            $letraDNI = $cadena[$resto];
            if ($letraDNI == strtoupper($letra)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El :attribute no es válido.';
    }
}
