<?php

namespace App\Imports;

use App\Models\Ciclo;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Str;

class CiclosImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Ciclo([
            'nombre' => $row[0],
            'slug' => Str::slug($row[0]),
            'seccion' => $row[1],
            'link' => $row[2],
        ]);
    }
}
