<?php

namespace App\Policies;

use App\Models\Proyecto;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProyectoPolicy
{
    use HandlesAuthorization;

    public function author(User $user, Proyecto $proyecto){
        if($user->id == $proyecto->user_id){
            return true;
        }else{
            return false;
        }
    }

    public function esPublico(?User $user,Proyecto $proyecto){
        if($proyecto->publico){
            return true;
        }else{
            return false;
        }
    }
}
