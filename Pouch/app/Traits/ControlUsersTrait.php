<?php

namespace App\Traits;

use App\Models\ControlUser;

trait ControlUsersTrait{
    public function registrarControlUsers(int $proyecto_id, string $accion){
        ControlUser::create([
            'user_id' => auth()->user()->id,
            'proyecto_id' => $proyecto_id,
            'accion' => $accion
        ]);
    }
}