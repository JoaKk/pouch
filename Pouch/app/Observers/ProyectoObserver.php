<?php

namespace App\Observers;

use App\Models\Proyecto;
use Illuminate\Support\Facades\Storage;

class ProyectoObserver
{

    public function creating(Proyecto $proyecto)
    {
        if (! \App::runningInConsole()) {
            $proyecto->user_id = auth()->user()->id;
        }
    }


    public function deleting(Proyecto $proyecto)
    {
        if ($proyecto->recursos) {
            foreach ($proyecto->recursos as $recurso) {
                Storage::delete($recurso->ruta_fichero);
            }
        }
    }
}
