<!DOCTYPE HTML>
<html>

<head>
    <title>POUCH | Repositorio</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <noscript>
        <link rel="stylesheet" href="assets/css/noscript.css" />
    </noscript>
    <link rel="icon" href="{{ asset('favicons/favicon.ico') }}">
</head>

<body class="is-preload landing">
    <div id="page-wrapper">
        <!-- Header -->
        <header id="header">
            <h1 id="logo"><a href="#">IES Miguel Herrero</a></h1>
            <nav id="nav">
                <ul>
                    <li><a href="#inicio">Inicio</a></li>
                    <li><a href="#servicios">Servicios</a></li>
                    <li><a href="#contacto">Contactanos</a></li>
                    <li><a href="{{ route('login') }}" class="button primary">Iniciar Sesión</a></li>
                </ul>
            </nav>
        </header>
        <section id="banner">
            <div class="content">
                <header style="text-align: left;">
                    <span><img src="{{ asset('images/lemapouch.png') }}" alt="" /></span>
                    <br>
                    <a href="{{ route('login') }}" class="button login" style="margin-left: 22%">ENTRAR</a>
                </header>
                <span class="image"><img src="{{ asset('vendor\adminlte\dist\img\logopouch.png') }}" alt="" /></span>
            </div>

            <a href="#servicios" class="goto-next scrolly">Descubre más</a>
        </section>
        <section id="servicios" class="wrapper style1 special fade-up">
            <div class="container">
                <header class="major">
                    <h2>Servicios del Repositorio POUCH</h2>
                    <p>Os mostramos un esquema de todos los servicios que ofrece la web</p>
                </header>
                <div class="box alt">
                    <div class="row gtr-uniform">
                        <section class="col-4 col-6-medium col-12-xsmall">
                            <span class="icon solid alt major fa-cloud-upload-alt"></span>
                            <h3>Almacena</h3>
                            <p>Sube todos los recursos de tu proyecto personal en nuestro repositorio.</p>
                        </section>
                        <section class="col-4 col-6-medium col-12-xsmall">
                            <span class="icon solid alt major fa-search"></span>
                            <h3>Explora</h3>
                            <p>Busca proyectos de cualquier ciclo formativo de otros alumnos.</p>
                        </section>
                        <section class="col-4 col-6-medium col-12-xsmall">
                            <span class="icon solid alt major fa-share-alt"></span>
                            <h3>Comparte</h3>
                            <p>Comparte tu proyecto final con otros alumnos para ayudarles en el desarrollo de su
                                proyecto.</p>
                        </section>
                        <section class="col-4 col-6-medium col-12-xsmall">
                            <span class="icon solid alt major fa-table"></span>
                            <h3>Organiza</h3>
                            <p>Organiza tu proyecto creando tareas mediante el sistema de tarjetas "Kanban".</p>
                        </section>
                        <section class="col-4 col-6-medium col-12-xsmall">
                            <span class="icon solid alt major fa-file-download"></span>
                            <h3>Descarga</h3>
                            <p>Descarga los recursos de los proyectos.</p>
                        </section>
                        <section class="col-4 col-6-medium col-12-xsmall">
                            <span class="icon solid alt major fa-pencil-alt"></span>
                            <h3>Evalúa</h3>
                            <p>Los profesores pueden evaluar los proyectos de los alumnos mediante una rúbrica.</p>
                        </section>
                    </div>
                </div>

                <footer class="major">
                    <ul class="actions special">
                        <a href="#contacto" class="goto-next scrolly">Next</a>
                    </ul>
                </footer>


            </div>

        </section>
        <!--
        <section id="one" class="spotlight style1 bottom">
            <span class="image fit main"><img src="images/pic02.jpg" alt="" /></span>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-4 col-12-medium">
                            <header>
                                <h2>Innovador repositorio virtual de proyectos</h2>
                                <p>Dedicado a los ciclos formativos de Cantabria</p>
                            </header>
                        </div>
                        <div class="col-4 col-12-medium">
                            <p>Feugiat accumsan lorem eu ac lorem amet sed accumsan donec.
                                Blandit orci porttitor semper. Arcu phasellus tortor enim mi
                                nisi praesent dolor adipiscing. Integer mi sed nascetur cep aliquet
                                augue varius tempus lobortis porttitor accumsan consequat
                                adipiscing lorem dolor.</p>
                        </div>
                        <div class="col-4 col-12-medium">
                            <p>Morbi enim nascetur et placerat lorem sed iaculis neque ante
                                adipiscing adipiscing metus massa. Blandit orci porttitor semper.
                                Arcu phasellus tortor enim mi mi nisi praesent adipiscing. Integer
                                mi sed nascetur cep aliquet augue varius tempus. Feugiat lorem
                                ipsum dolor nullam.</p>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#two" class="goto-next scrolly">Next</a>
        </section>
        <section id="two" class="spotlight style2 right">
            <span class="image fit main"><img src="images/pic03.jpg" alt="" /></span>
            <div class="content">
                <header>
                    <h2>Interdum amet non magna accumsan</h2>
                    <p>Nunc commodo accumsan eget id nisi eu col volutpat magna</p>
                </header>
                <p>Feugiat accumsan lorem eu ac lorem amet ac arcu phasellus tortor enim mi mi nisi praesent
                    adipiscing. Integer mi sed nascetur cep aliquet augue varius tempus lobortis porttitor lorem et
                    accumsan consequat adipiscing lorem.</p>
                <ul class="actions">
                    <li><a href="#" class="button">Learn More</a></li>
                </ul>
            </div>
            <a href="#three" class="goto-next scrolly">Next</a>
        </section>
        <section id="three" class="spotlight style3 left">
            <span class="image fit main bottom"><img src="images/pic04.jpg" alt="" /></span>
            <div class="content">
                <header>
                    <h2>Interdum felis blandit praesent sed augue</h2>
                    <p>Accumsan integer ultricies aliquam vel massa sapien phasellus</p>
                </header>
                <p>Feugiat accumsan lorem eu ac lorem amet ac arcu phasellus tortor enim mi mi nisi praesent
                    adipiscing. Integer mi sed nascetur cep aliquet augue varius tempus lobortis porttitor lorem et
                    accumsan consequat adipiscing lorem.</p>
                <ul class="actions">
                    <li><a href="#" class="button">Learn More</a></li>
                </ul>
            </div>
            <a href="#contacto" class="goto-next scrolly">Next</a>
        </section>
    -->
        <section id="contacto" class="wrapper style2 special fade">
            <div class="container">
                <header>
                    <h2>¡Contacta con nosotros!</h2>
                    <p>Déjanos cualquier tipo de sugerencia o problema relacionado con la web.</p>
                </header>
                <form method="post" action="#" class="cta">
                    @csrf
                    <div class="row gtr-50">
                        <div id="firstChild" class="col-6">
                            <input type="text" name="nombreContacto" id="nombreContacto"
                                placeholder="Nombre y apellidos" />
                        </div>
                        <div class="col-6">
                            <input type="email" name="email" id="email" placeholder="Email de contacto" />
                        </div>
                    </div>
                    <br>
                    <div class="row gtr-50">
                        <select name="categoria" id="categoria">
                            <option value="default" disabled selected> - Seleccione una categoría - </option>
                            <option value="1"> Opción 1 </option>
                            <option value="2"> Opción 2 </option>
                            <option value="3"> Opción 3 </option>
                            <option value="4"> Opción 4 </option>
                        </select>
                    </div>
                    <br>
                    <div class="row gtr-50">
                        <textarea name="" id="" cols="30" rows="4" placeholder="Escribe tu comentario..."></textarea>
                    </div>
                    <br>
                    <div class="row gtr-50">
                        <input type="submit" value="ENVIAR" class="fit primary">
                    </div>
                </form>
            </div>
        </section>
        <footer id="footer">
            <ul class="icons">
                <li><a href="https://twitter.com/Pouch00739144" class="icon brands alt fa-twitter"><span class="label">Twitter</span></a></li>
                <li><a href="#" class="icon brands alt fa-facebook-f"><span class="label">Facebook</span></a></li>
                <li><a href="#" class="icon brands alt fa-linkedin-in"><span class="label">LinkedIn</span></a></li>
                <li><a href="#" class="icon brands alt fa-instagram"><span class="label">Instagram</span></a></li>
                <li><a href="#" class="icon brands alt fa-github"><span class="label">GitHub</span></a></li>
                <li><a href="#" class="icon solid alt fa-envelope"><span class="label">Email</span></a></li>
            </ul>
            <ul class="copyright">
                <li>&copy; 2021. Todos los derechos reservados. POUCH&trade;</li>
                <li>Autor: <a href="http://html5up.net">Joaquín Blanco</a></li>
            </ul>
        </footer>

    </div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.scrolly.min.js"></script>
    <script src="assets/js/jquery.dropotron.min.js"></script>
    <script src="assets/js/jquery.scrollex.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>
