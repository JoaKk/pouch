@extends('adminlte::page')

@section('title', 'Editar Proyecto')

@section('content')
    <div class="card">
        <div class="card-header bg-primary p-0 py-2">
            <section class="content-header">
                <h3 class="card-title text-bold"><span class="fas fa-fx fa-pencil-alt"></span> EDITA TU PROYECTO PERSONAL</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('proyectos.personales') }}">Mis
                            proyectos</a></li>
                    <li class="breadcrumb-item active text-white-50">Edita tu proyecto</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            {!! Form::model($proyecto, ['route' => ['proyectos.update', $proyecto], ' autocomplete' => 'off', 'files' => true, 'method' => 'put']) !!}
            @csrf

            @include('proyectos.layouts.form')

            {!! Form::submit('Actualizar proyecto', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/app.css">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <style>
        .custom-file-input~.custom-file-label::after {
            content: "Elegir";
        }

    </style>
@stop

@section('js')
    <script src="{{ asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#nombre").stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: '#slug',
                space: '-'
            });
            $('.js-example-basic-single').select2();
        });

    </script>
    <script>
        $('select').selectpicker();
        $('.selectpicker').selectpicker({
            noneResultsText: 'No se han encontrado resultados'
        });

    </script>
    <script>
        //Cambiar imagen
        document.getElementById("customFileLang3").addEventListener('change', cambiarImagen);

        function cambiarImagen(event) {
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("imagenLogo").setAttribute('src', event.target.result);
            };

            reader.readAsDataURL(file);
        }

    </script>
    @if (session('updateInfo'))
        <script>
            Swal.fire(
                'Actualizado!',
                'Tu proyecto ha sido actualizado con éxito.',
                'success'
            )

        </script>
    @endif
    <script>
        //Rellenar input file si hay recursos
        const recursos = {!! json_encode($proyecto->recursos) !!}
        const logo = {!! json_encode($proyecto->ruta_logo) !!}

        var array = logo.split("/");
        var fileName = array[array.length - 1];
        $('#custom-label-logo').html(fileName);

        if (recursos.length > 0) {
            recursos.forEach(obj => {
                var array = obj.ruta_fichero.split("/");
                var fileName = array[array.length - 1];
                if (obj.tipo_id == 1 || obj.tipo_id == 2) {
                    $('#custom-label-file').html(fileName);
                } else if (obj.tipo_id == 3 || obj.tipo_id == 4) {
                    $('#custom-label-doc').html(fileName);
                }
            });
        }

    </script>
    <script>
        $('#customFileLang').on('change', function() {
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
            $(this).next('#custom-label-file').html(cleanFileName);
        })
        $('#customFileLang2').on('change', function() {
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
            $(this).next('#custom-label-doc').html(cleanFileName);
        })
        $('#customFileLang3').on('change', function() {
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
            $(this).next('#custom-label-logo').html(cleanFileName);
        })

    </script>
@stop
