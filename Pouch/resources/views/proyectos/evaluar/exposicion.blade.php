@extends('adminlte::page')

@section('title', 'Proyectos - Evaluar')

@section('content')
    @livewire('proyecto.proyectos-evaluar-exposicion',['proyecto' => $proyecto])
@stop
@section('css')

    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type=number]{
            font-size: 1.3em;
            font-weight: 500;
        }
    </style>
@stop

@section('js')
    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })

    </script>
    <script>
        livewire.on('closeModal', function(rubricaId) {
            $("#myModal" + rubricaId).modal('hide');
        });

    </script>
    <script>
        $("td button").click(function(e) {
            var id = ($(this).attr("id").split(".")[1]);
            $('#myModal' + id).modal();
            $('input[name=inlineRadioOptions' + id + ']').click(function() {
                $(".modal-footer #btn" + id).removeAttr('disabled');
            });
            $('.modal-footer #closeBtn' + id).click(function(e) {
                $('input[name=inlineRadioOptions' + id + ']').prop('checked',false);
                $(".modal-footer #btn" + id).prop('disabled',true);
            });
        });

    </script>
@stop