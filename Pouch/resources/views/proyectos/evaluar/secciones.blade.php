@extends('adminlte::page')

@section('title', 'Proyectos - Evaluar')

@section('content')
    <div class="card">
        <div class="card-header bg-dark p-0 py-2">
            <section class="content-header">
                <h3 class="card-title text-bold"><span class="fas fa-fx fa-edit"></span> LISTA DE SECCIONES DE LAS
                    DIFERENTES RÚBRICAS</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item"><a class="text-white"
                            href="{{ route('proyectos.evaluar.index') }}">Evaluar Proyecto</a></li>
                    <li class="breadcrumb-item active text-white-50">Secciones</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col text-center">
                    <p class="h5">Nombre del proyecto:</p>
                    <p class="text-bold h3">{{ $proyecto->nombre }}</p>
                </div>
                <div class="col text-center">
                    <div class="row">
                        <div class="col">
                            <p class="h5">Puntuación total del proyecto:</p>
                            <p class="text-bold h3">
                                @if (empty($proyecto->puntuacion))
                                    <small><b class="text-muted">Faltan profesores por evaluar</b> </small>
                                @else
                                    {{ $proyecto->puntuacion }} / 100
                                @endif
                            </p>
                        </div>
                        <div class="col">
                            <p class="h5">Evaluación personal del proyecto:</p>
                            <p class="text-bold h3">
                                @if (DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->count() != 4)
                                    <small><b class="text-muted"> No se han evaluado todas las secciones</b> </small>
                                @else
                                    {{ DB::table('calificaciones_proyecto')->where('user_id', auth()->user()->id)->where('proyecto_id', $proyecto->id)->sum('puntuacion') }}
                                    / 100
                                @endif
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row" style="margin-top: 40px">
                <div class="col">
                    <div class="info-box">
                        <span class="info-box-icon bg-secondary elevation-1"><i
                                class="fas fa-chalkboard-teacher"></i></span>
                        <div class="info-box-content">
                            <div class="row">
                                <div class="col border-right">
                                    <div class="row h-100">
                                        <div class="col m-auto"><span class="info-box-text  text-left">
                                                <h2>
                                                    Exposición (15%)
                                                </h2>
                                            </span>
                                        </div>
                                        <div class="col m-auto text-center">
                                            <h2><a class="btn btn-secondary btn-lg"
                                                    href="{{ route('proyecto.evaluarExposicion', $proyecto) }}">Evaluar <i
                                                        class="fas fa-sign-in-alt fa-lg"></i></a>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <p><b>Rúbricas Evaluadas:</b> {{ $numeroRubricas[1]['rubricasCompletadas'] }}
                                                /{{ $numeroRubricas[1]['rubricasDisponibles'] }}</p>
                                            <p><b>Puntuación total:</b>
                                                @if (empty($numeroRubricas[1]['puntuacion']))
                                                    -
                                                @else
                                                    {{ $numeroRubricas[1]['puntuacion'] }}
                                                @endif / 15
                                            </p>
                                        </div>
                                        <div class="col m-auto">
                                            @if ($numeroRubricas[1]['rubricasCompletadas'] == 0)
                                                <h4 class="text-yellow"><b>SIN EVALUAR</b></h4>
                                            @elseif(($numeroRubricas[1]['rubricasCompletadas'] > 0 &&
                                                $numeroRubricas[1]['rubricasCompletadas'] <
                                                    $numeroRubricas[1]['rubricasDisponibles']) ||
                                                    !$numeroRubricas[1]['finalizado']) <h4 class="text-red intermitent">
                                                    <b>EN PROGRESO</b></h4>
                                                @elseif($numeroRubricas[1]['finalizado'])
                                                    <h4 class="text-green"><b>EVALUADO</b></h4>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100"></div>
                <div class="col">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-file-alt"></i></span>
                        <div class="info-box-content">
                            <div class="row">
                                <div class="col border-right">
                                    <div class="row h-100">
                                        <div class="col m-auto"><span class="info-box-text  text-left">
                                                <h2>
                                                    Documentación (40%)
                                                </h2>
                                            </span>
                                        </div>
                                        <div class="col m-auto text-center">
                                            <h2><a class="btn btn-danger btn-lg"
                                                    href="{{ route('proyecto.evaluarDocumentacion', $proyecto) }}">Evaluar
                                                    <i class="fas fa-sign-in-alt fa-lg"></i></a>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <p><b>Rúbricas Evaluadas:</b> {{ $numeroRubricas[2]['rubricasCompletadas'] }}
                                                /{{ $numeroRubricas[2]['rubricasDisponibles'] }}</p>
                                            <p><b>Puntuación total:</b>
                                                @if (empty($numeroRubricas[2]['puntuacion']))
                                                    -
                                                @else
                                                    {{ $numeroRubricas[2]['puntuacion'] }}
                                                @endif / 40
                                            </p>
                                        </div>
                                        <div class="col m-auto">
                                            @if ($numeroRubricas[2]['rubricasCompletadas'] == 0)
                                                <h4 class="text-yellow"><b>SIN EVALUAR</b></h4>
                                            @elseif(($numeroRubricas[2]['rubricasCompletadas'] > 0 &&
                                                $numeroRubricas[2]['rubricasCompletadas'] <
                                                    $numeroRubricas[2]['rubricasDisponibles']) ||
                                                    !$numeroRubricas[2]['finalizado']) <h4 class="text-red intermitent">
                                                    <b>EN PROGRESO</b></h4>
                                                @elseif($numeroRubricas[2]['finalizado'])
                                                    <h4 class="text-green"><b>EVALUADO</b></h4>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100"></div>

                <div class="col">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-tablet-alt"></i></span>
                        <div class="info-box-content">
                            <div class="row">
                                <div class="col border-right">
                                    <div class="row h-100">
                                        <div class="col m-auto"><span class="info-box-text  text-left">
                                                <h2>
                                                    Aplicación (30%)
                                                </h2>
                                            </span>
                                        </div>
                                        <div class="col m-auto text-center">
                                            <h2><a class="btn btn-success btn-lg"
                                                    href="{{ route('proyecto.evaluarAplicacion', $proyecto) }}">Evaluar
                                                    <i class="fas fa-sign-in-alt fa-lg"></i></a>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <p><b>Rúbricas Evaluadas:</b> {{ $numeroRubricas[3]['rubricasCompletadas'] }}
                                                /{{ $numeroRubricas[3]['rubricasDisponibles'] }}</p>
                                            <p><b>Puntuación total:</b>
                                                @if (empty($numeroRubricas[3]['puntuacion']))
                                                    -
                                                @else
                                                    {{ $numeroRubricas[3]['puntuacion'] }}
                                                @endif / 30
                                            </p>
                                        </div>
                                        <div class="col m-auto">
                                            @if ($numeroRubricas[3]['rubricasCompletadas'] == 0)
                                                <h4 class="text-yellow"><b>SIN EVALUAR</b></h4>
                                            @elseif(($numeroRubricas[3]['rubricasCompletadas'] > 0 &&
                                                $numeroRubricas[3]['rubricasCompletadas'] <
                                                    $numeroRubricas[3]['rubricasDisponibles']) ||
                                                    !$numeroRubricas[3]['finalizado']) <h4 class="text-red intermitent">
                                                    <b>EN PROGRESO</b></h4>
                                                @elseif($numeroRubricas[1]['finalizado'])
                                                    <h4 class="text-green"><b>EVALUADO</b></h4>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100"></div>
                <div class="col">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-eye"></i></span>
                        <div class="info-box-content">
                            <div class="row">
                                <div class="col border-right">
                                    <div class="row h-100">
                                        <div class="col m-auto"><span class="info-box-text  text-left">
                                                <h2>
                                                    Seguimiento (15%)
                                                </h2>
                                            </span>
                                        </div>
                                        <div class="col m-auto  text-center">
                                            <h2><a class="btn btn-warning btn-lg"
                                                    href="{{ route('proyecto.evaluarSeguimiento', $proyecto) }}">Evaluar
                                                    <i class="fas fa-sign-in-alt fa-lg"></i></a>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <p><b>Rúbricas Evaluadas:</b> {{ $numeroRubricas[4]['rubricasCompletadas'] }}
                                                /{{ $numeroRubricas[4]['rubricasDisponibles'] }}</p>
                                            <p><b>Puntuación total:</b>
                                                @if (empty($numeroRubricas[4]['puntuacion']))
                                                    -
                                                @else
                                                    {{ $numeroRubricas[4]['puntuacion'] }}
                                                @endif / 15
                                            </p>
                                        </div>
                                        <div class="col m-auto">
                                            @if ($numeroRubricas[4]['rubricasCompletadas'] == 0)
                                                <h4 class="text-yellow"><b>SIN EVALUAR</b></h4>
                                            @elseif(($numeroRubricas[4]['rubricasCompletadas'] > 0 &&
                                                $numeroRubricas[4]['rubricasCompletadas'] <
                                                    $numeroRubricas[4]['rubricasDisponibles']) ||
                                                    !$numeroRubricas[4]['finalizado']) <h4 class="text-red intermitent">
                                                    <b>EN PROGRESO</b></h4>
                                                @elseif($numeroRubricas[4]['finalizado'])
                                                    <h4 class="text-green"><b>EVALUADO</b></h4>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style>
        .intermitent {
            animation: blinker 1s linear infinite;
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }

    </style>
@stop

@section('js')
    @if (session('mensajeExposicionFinalizado'))
        <script>
            Swal.fire(
                '¡Se ha evaluado la exposición correctamente!',
                '',
                'success'
            )

        </script>
    @elseif(session('mensajeDocumentacionFinalizado'))
        <script>
            Swal.fire(
                '¡Se ha evaluado la documentación correctamente!',
                '',
                'success'
            )

        </script>
    @elseif(session('mensajeAplicacionFinalizado'))
        <script>
            Swal.fire(
                '¡Se ha evaluado la aplicación correctamente!',
                '',
                'success'
            )

        </script>
    @elseif(session('mensajeSeguimientoFinalizado'))
        <script>
            Swal.fire(
                '¡Se ha evaluado el seguimiento correctamente!',
                '',
                'success'
            )

        </script>
    @elseif(session('mensajeError'))
        <script>
            Swal.fire(
                '¡Ups! Ha ocurrido un problema. Usted no debería cambiar el código html &#128545;',
                '',
                'error'
            )

        </script>
    @endif
    @php
    Session::forget('mensajeExposicionFinalizado');
    Session::forget('mensajeDocumentacionFinalizado');
    Session::forget('mensajeAplicacionFinalizado');
    Session::forget('mensajeSeguimientoFinalizado');
    Session::forget('mensajeError');
    @endphp
@stop
