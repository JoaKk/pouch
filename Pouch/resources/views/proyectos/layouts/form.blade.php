<div class="form-group">
    {!! Form::label('nombre', 'TÍTULO DEL PROYECTO: *') !!}
    {!! Form::text('nombre', null, [
    'class' => 'form-control',
    'placeholder' => 'Inserte el título del proyecto',
]) !!}
    <hr>
    @error('nombre')
        <small class="text-danger">{{ $message }}</small>
    @enderror
    @error('slug')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group hidden">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, [
    'class' => 'form-control',
    'placeholder' => 'Aquí se mostrará el slug del proyecto',
    'readonly',
]) !!}
    <hr>
    @error('slug')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="row">


    <div class="col form-group">
        {!! Form::label('ciclo_id', 'CICLO FORMATIVO: *') !!}
        <br>
        @isset($proyecto)
            <select class="selectpicker show-tick form-control" data-actions-box="true" data-live-search="true"
                name="ciclo_id">
                @php
                    $seccionAux = '';
                @endphp
                <option disabled> - Selecciona un ciclo formativo - </option>
                @foreach ($ciclos as $ciclo)
                    @if ($seccionAux != $ciclo->seccion)
                        </optgroup>
                        {{ $seccionAux = $ciclo->seccion }}
                        <optgroup label="{{ $ciclo->seccion }}">
                    @endif
                    @if ($ciclo->id == $proyecto->ciclo_id)
                        <option selected value="{{ $ciclo->id }}">{{ $ciclo->nombre }}</option>
                    @else
                        <option value="{{ $ciclo->id }}">{{ $ciclo->nombre }}</option>
                    @endif
                @endforeach
                </optgroup>
            </select>
        @else
            <select class="selectpicker show-tick form-control" data-actions-box="true" data-live-search="true"
                name="ciclo_id">
                @php
                    $seccionAux = '';
                @endphp
                <option disabled selected> - Selecciona un ciclo formativo - </option>
                @foreach ($ciclos as $ciclo)
                    @if ($seccionAux != $ciclo->seccion)
                        </optgroup>
                        {{ $seccionAux = $ciclo->seccion }}
                        <optgroup label="{{ $ciclo->seccion }}">
                    @endif
                    <option value="{{ $ciclo->id }}">{{ $ciclo->nombre }}</option>
                @endforeach
                </optgroup>
            </select>
        @endisset
        @error('ciclo_id')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>
    <div class="col form-group border-left">
        {!! Form::label('publico[]', 'PRIVACIDAD DEL PROYECTO:') !!}
        <br>
        @isset($proyecto)
            @if ($proyecto->publico)
                <input type="checkbox" name="publico[]" checked data-toggle="toggle" data-on="Público" data-off="Privado"
                    data-onstyle="success" data-offstyle="danger" data-height="40" data-width="105">
            @else
                <input type="checkbox" name="publico[]" data-toggle="toggle" data-on="Público" data-off="Privado"
                    data-onstyle="success" data-offstyle="danger" data-height="40" data-width="105">
            @endif
        @else
            <input type="checkbox" name="publico[]" checked data-toggle="toggle" data-on="Público" data-off="Privado"
                data-onstyle="success" data-offstyle="danger" data-height="40" data-width="105">
        @endisset

        @error('publico')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>
</div>
<hr>
<div class="form-group">
    {!! Form::label('descripcion', 'DESCRIPCIÓN DEL PROYECTO: *') !!}
    <p>Inserte una descripción general del proyecto. <b>¿Sobre que trata tu proyecto? ¿Para qué sirve?</b> </p>
    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '3']) !!}
    @error('descripcion')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<hr>

<div class="row mb-3">
    <div class="col">
        <div class="image-wrapper">
            @isset($proyecto->ruta_logo)
                @if ($proyecto->ruta_logo != 'images/defaultwhite.jpg')
                    <img id="imagenLogo" src="{{ Storage::url($proyecto->ruta_logo) }}" alt="">
                @else
                    <img id="imagenLogo" src="{{ asset('images/defaultwhite.jpg') }}" alt="">
                @endif
            @else
                <img id="imagenLogo" src="{{ asset('images/defaultwhite.jpg') }}" alt="">
            @endisset
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            {!! Form::label('ruta_logo', 'LOGO DEL PROYECTO') !!}
            <div class="custom-file">
                <input type="file" name="ruta_logo" class="custom-file-input cursor-pointer" id="customFileLang3"
                    lang="es" accept="image/*">
                <label class="custom-file-label" id="custom-label-logo" for="customFileLang3">Seleccionar
                    Archivo</label>
            </div>
        </div>

        <p>Selecciona el logo que quieres que aparezca en el proyecto. Por defecto, se mostrará el logo del repositorio POUCH.</p>
    </div>
</div>
<hr>
@error('ruta_logo')
    <small class="text-danger">{{ $message }}</small>
@enderror
<div class="form group">
    {!! Form::label('ruta_fichero', 'CARPETA CONTENEDORA DEL CÓDIGO DEL PROYECTO: ') !!}
    <small>(Tipo de archivo válido : <strong>.rar o .zip</strong>)</small>
    <div class="custom-file">
        <input type="file" name="ruta_fichero" class="custom-file-input cursor-pointer" id="customFileLang" lang="es"
            accept=".rar,.zip">
        <label class="custom-file-label" id="custom-label-file" for="customFileLang">Seleccionar Archivo</label>
    </div>
    <hr>
    @error('ruta_fichero')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>

<div class="form group">
    {!! Form::label('ruta_documentacion', 'DOCUMENTACIÓN DEL PROYECTO: ') !!}
    <small>(Tipo de archivo válido : <strong>.pdf o .doc</strong>)</small>
    <div class="custom-file">
        <input type="file" name="ruta_documentacion" class="custom-file-input cursor-pointer" id="customFileLang2"
            lang="es" accept=".pdf,.doc">
        <label class="custom-file-label" id="custom-label-doc" for="customFileLang2">Seleccionar Archivo</label>
    </div>
    <hr>
    @error('ruta_documentacion')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
