@extends('adminlte::page')

@section('title', 'Detalle Proyecto')

@section('content')

    <div class="card">
        <div class="card-header bg-success p-0 py-2">
            <section class="content-header">
                <h3 class="card-title text-bold uppercase"><i class="fas fa-fx fa-tags"></i>
                    {{ $proyecto->nombre }}
                </h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('proyectos.index') }}">Lista de
                            proyectos</a></li>
                    <li class="breadcrumb-item active text-white-50">Detalle del proyecto</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            @if ($proyecto->publico ||
        $proyecto->user->id == auth()->user()->id ||
        auth()->user()->adminlte_desc() == 'Administrador')
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="mb-4">Linea temporal del proyecto</h4>
                                <div class="timeline">

                                    @php
                                        $fechaAux = ''; // Carbon\Carbon::now()->format('d-F-Y');
                                    @endphp
                                    @foreach ($ultimosRegistros as $registro)
                                        @if ($registro->created_at->format('d-F-Y') != $fechaAux)
                                            @php
                                                $fechaAux = $registro->created_at->format('d-F-Y');
                                                $array = ['red', 'yellow', 'green', 'blue'];
                                            @endphp
                                            <div class="time-label">
                                                <span class="bg-dark">{{ $fechaAux }}</span>
                                            </div>
                                        @endif

                                        <div>
                                            @switch($registro->accion)
                                                @case($registro->accion == 'PROYECTO CREADO')
                                                    <i class="fas fa-cloud-upload-alt bg-yellow"></i>
                                                @break
                                                @case(Str::contains($registro->accion, 'LOGO'))
                                                    <i class="fas fa-image bg-maroon"></i>
                                                @break
                                                @case(Str::contains($registro->accion,'CÓDIGO'))
                                                    <i class="fas fa-code bg-green"></i>
                                                @break
                                                @case(Str::contains($registro->accion,'DOCUMENTACIÓN'))
                                                    <i class="fas fa-file-pdf bg-blue"></i>
                                                @break
                                                @default

                                            @endswitch
                                            <div class="timeline-item">
                                                <span class="time">
                                                    <i class="fas fa-clock"></i>
                                                    {{ $registro->created_at->format('H:i') }}</span>
                                                <h3 class="timeline-header text-md">{{ $registro->accion }}
                                                </h3>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div>
                                        <i class="fas fa-clock bg-gray"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                <h4>Comentarios</h4>
                                @if ($proyecto->comentarios->count())
                                    <div class="card pt-2">

                                        @foreach ($proyecto->comentarios as $comentario)
                                            <div class="post ml-2">
                                                <div class="row">
                                                    <div class="user-block col">
                                                        <a href="{{ route('perfil', $comentario->getUsuario()) }}"><img
                                                                class="img-circle img-bordered-sm"
                                                                src="{{ $comentario->getUsuario()->adminlte_image() }}"
                                                                alt="user image"></a>
                                                        <div>
                                                            <span class="username">
                                                                <a
                                                                    href="{{ route('perfil', $comentario->getUsuario()) }}">{{ $comentario->getUsuario()->name }}</a>
                                                            </span>
                                                            <span class="description"><i class="far fa-clock"></i> Publicado
                                                                hace
                                                                {{ $proyecto->calcularTiempo($comentario->created_at) }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        @if ($comentario->user_id == auth()->user()->id)
                                                            <div style="top:0;right: 0;">
                                                                <button
                                                                    class="comentario-eliminar btn btn-danger float-right"
                                                                    data-href="{{ route('proyectos.eliminarComentario', $comentario) }}">
                                                                    <i class="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <p>
                                                    {{ $comentario->descripcion }}
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="card pt-2">
                                        <div class="row">
                                            <p class="text-center m-auto">
                                                <strong>¡Sé el primero en comentar este proyecto!</strong>
                                            </p>
                                        </div>
                                    </div>
                                @endif

                            </div>
                            <form action="{{ route('proyectos.comentar', $proyecto) }}" method="post"
                                class="input-group  input-group-sm mb-0 ml-2">
                                @csrf
                                <input autocomplete="off" name="comentario" id="comentario"
                                    class="rounded shadow-lg form-control form-control-sm"
                                    placeholder="Escribir comentario">
                                <div class="input-group-append">
                                    <button type="submit" id="enviarComentario" class="btn btn-danger">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                        @if ($proyecto->ruta_logo == 'images/defaultwhite.jpg')
                            <img class="h-90 w-100" src="{{ asset('images/defaultwhite.jpg') }}" alt="">
                        @else
                            <img class="h-80 w-100" src="{{ Storage::url($proyecto->ruta_logo) }}" alt="">
                        @endif
                        <br>
                        <div class="text-muted">
                            <p class="text-md"><i class="fas fa-file-alt"></i> Descripción del proyecto :
                                <b class="text-muted text-md">{!! $proyecto->descripcion !!}</b>
                            </p>
                        </div>
                        <br>
                        <div class="text-muted">

                            <p class="text-md"><i class="fas fa-user"></i> Autor/a :
                                <a href="{{ route('perfil', $proyecto->user) }}">
                                    <b>{{ $proyecto->user->name }}</b>
                                </a>
                            </p>
                            <p class="text-md"><i class="fas fa-graduation-cap"></i> Ciclo Formativo :
                                <b>{{ $proyecto->ciclo->nombre }}</b>
                            </p>
                            <p class="text-md"><i class="fas fa-fx fa-cloud-download-alt"></i> Descargas :
                                <b>{{ $proyecto->descargas }}</b>
                            </p>
                            <p class="text-md"><i class="fas fa-calendar-alt"></i> Creado el :
                                <b>{{ $proyecto->created_at->format('d-m-Y') }}</b>
                            </p>
                        </div>

                        <h5 class="mt-5 text-muted">Archivos del proyecto</h5>
                        <ul class="list-unstyled">
                            @php
                                $logoSplit = explode('/', $proyecto->ruta_logo);
                                $nombreLogo = $logoSplit[count($logoSplit) - 1];
                            @endphp
                            @foreach ($proyecto->recursos as $recurso)
                                @php
                                    $recursoSplit = explode('/', $recurso->ruta_fichero);
                                    $nombreArchivo = $recursoSplit[count($recursoSplit) - 1];
                                @endphp
                                @switch($recurso->tipo_id)
                                    @case(1)
                                        <!-- .rar -->
                                        <li>
                                            <a href="{{ route('proyectos.descargarRecurso', [$proyecto, $recurso]) }}"
                                                class="btn-link text-secondary"><i class="far fa-fw fa-folder-open"></i>
                                                {{ $nombreArchivo }} </a>
                                        </li>
                                    @break
                                    @case(2)
                                        <!-- .zip -->
                                        <li>
                                            <a href="{{ route('proyectos.descargarRecurso', [$proyecto, $recurso]) }}"
                                                class="btn-link text-secondary"><i class="far fa-fw fa-file-archive"></i>
                                                {{ $nombreArchivo }}</a>
                                        </li>
                                    @break
                                    @case(3)
                                        <!-- .pdf -->
                                        <li>
                                            <a href="{{ route('proyectos.descargarRecurso', [$proyecto, $recurso]) }}"
                                                class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i>
                                                {{ $nombreArchivo }}</a>
                                        </li>
                                    @break
                                    @case(4)
                                        <!-- .doc -->
                                        <li>
                                            <a href="{{ route('proyectos.descargarRecurso', [$proyecto, $recurso]) }}"
                                                class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i>
                                                {{ $nombreArchivo }}</a>
                                        </li>
                                    @break
                                    @default

                                @endswitch
                            @endforeach
                            <li>
                                <a href="{{ route('proyectos.descargarLogo', $proyecto) }}"
                                    class="btn-link text-secondary"><i class="far fa-fw fa-image "></i>
                                    {{ $nombreLogo }}</a>
                            </li>
                        </ul>
                        <div class="text-center mt-5 mb-3">
                            <a href="{{ route('proyectos.download', $proyecto) }}"
                                class="btn btn-sm btn-primary">Descargar
                                Proyecto</a>
                            @if ($proyecto->user_id == auth()->user()->id)
                                <a href="#" data-href="" id="crear-version" class="btn btn-sm btn-success ">Crear
                                    versión</a>
                            @endif
                            <a href="#" class="btn btn-sm btn-warning disabled">Ejecutar proyecto</a>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-danger" role="alert">
                    <span><i class="fas fa-lock"></i> Este proyecto es <strong>PRIVADO</strong></span>
                </div>
            @endif
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    @if (session('createInfo'))
        <script>
            Swal.fire(
                '¡Genial!',
                'Ya tenemos tu proyecto en nuestro repositorio.',
                'success'
            )

        </script>
    @endif
    @if (session('eliminarComentarioInfo'))
        <script>
            Swal.fire(
                '¡Comentario eliminado!',
                '',
                'success'
            )

        </script>
    @endif
    <script>
        $('.comentario-eliminar').click(function(e) {
            e.preventDefault();

            Swal.fire({
                title: '¿Desea eliminar el comentario?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, eliminar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    thisdata = $(this).attr('data-href');
                    window.location.href = thisdata;
                }
            })
        });

    </script>
    <script>
        //validar comentario
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "4000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        $('#enviarComentario').click(function(e) {
            if ($('#comentario').val() == "") {
                e.preventDefault();
                toastr["warning"]("Inserte al menos una palabra.");
            }
        });

    </script>
    <script>
        $('#crear-version').click(function(e) {
            e.preventDefault();
            Swal.fire({
                title: 'Crear una versión del proyecto',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off',
                    placeholder: 'Nombre de la versión. Ej: Proyecto 1.0'
                },
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Crear',
                cancelButtonText: 'Cancelar',
                customClass: {
                    validationMessage: 'my-validation-message'
                },
                preConfirm: (value) => {
                    if (!value) {
                        Swal.showValidationMessage(
                            'Indique un nombre de versión'
                        )
                    } else {
                        Swal.fire(
                            `¡Versión ${value} creada!`,
                            '',
                            'success'
                        )
                        /*thisdata = $(this).attr('data-href');
                        window.location.href = thisdata;*/
                    }

                }
            })
        });

    </script>
@stop
