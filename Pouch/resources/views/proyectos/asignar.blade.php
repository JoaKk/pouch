@extends('adminlte::page')

@section('title', 'Asignar Usuario')

@section('content')

    <head>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    </head>
    <div class="card">
        <div class="card-header bg-dark p-0 py-2">
            <section class="content-header">
                <h3 class="card-title text-bold"><span class="fas fa-fx fa-user-plus"></span> ASIGNAR PROFESORES</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('proyectos.personales') }}">Mis Proyectos</a></li>
                    <li class="breadcrumb-item active text-white-50">Asignar profesores</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            <p class="h5">Nombre del proyecto:</p>
            <p class="form-control">{{ $proyecto->nombre }}</p>
            <p class="h5">Profesores asignados:</p>
            @if (count($usuariosAsignados) != 0)
                @foreach ($usuariosAsignados as $user)

                    <span class="bg-green p-1 mr-1 rounded">{{ $user->name }}
                        <button class="btn eliminar-asociado pr-0" data-toggle="tooltip" data-placement="top" title="Eliminar profesor"
                            data-href="{{ route('proyectos.desasignar', [$proyecto, $user]) }}">
                            <i class="fas fa-times"></i>
                        </button>
                    </span>

                @endforeach
            @else
                <span class="bg-yellow p-1 mr-1 rounded">No hay profesores asignados</span>
            @endif
            <h2 class="h5 mt-2">Listado de profesores</h2>
            <form action="{{ route('proyectos.asignar', $proyecto) }}" method="POST">
                @csrf
                <div>
                    <select class="selectpicker show-tick form-control" data-dropup-auto="false" data-actions-box="true" data-live-search="true"
                        title="Ningún profesor seleccionado" data-select-all-text="Seleccionar todos"
                        data-deselect-all-text="Eliminar seleccionados" data-width="30%" name="users[]" multiple>
                        @if (count($profesoresDisponibles))
                            @foreach ($profesoresDisponibles as $profesorDisponible)
                                <option value="{{ $profesorDisponible->id }}">{{ $profesorDisponible->name }}</option>
                            @endforeach
                        @else
                            <option class="text-center" value="" disabled>No hay profesores disponibles</option>
                        @endif

                    </select>
                </div>
                <br>
                <button class="btn btn-primary">Asignar usuarios</button>
            </form>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $('select').selectpicker();

    </script>
    @if (session('desasociarInfo'))
        <script>
            Swal.fire(
                '¡Profesor desasignado con éxito!',
                '',
                'success'
            )

        </script>
    @endif
    @if (session('desasociarInfoError'))
        <script>
            Swal.fire(
                '¡Ups! Algo no funciona...',
                'Intentelo de nuevo',
                'error'
            )

        </script>
    @endif
    @if (session('asociarInfo'))
        <script>
            Swal.fire(
                '¡Asignados con éxito!',
                'Se han asignado los usuarios al proyecto',
                'success'
            )

        </script>
    @endif
    <script>
        $('.eliminar-asociado').click(function(e) {
            e.preventDefault();

            Swal.fire({
                title: '¿Desea desasignar el profesor a este proyecto?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    thisdata = $(this).attr('data-href');
                    window.location.href = thisdata;
                }
            })
        });

    </script>
       <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })

    </script>
@stop
