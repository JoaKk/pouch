<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div>
                <h1 class="text-center">
                    <img class="h-20 m-auto" src="{{asset('vendor/adminlte/dist/img/logopouch.png')}}" alt="">
                </h1>
            </div>
            <div class="row">
                <div class="col">
                    <x-jet-label for="name" value="Nombre *" />
                    <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                </div>
                <div class="col">
                    <x-jet-label for="email" value="Correo electrónico *" />
                    <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
                </div>
            </div>
            <div class="row">
                <div class="mt-4 col">
                    <x-jet-label for="localidad" value="Localidad" />
                    <x-jet-input id="localidad" class="block mt-1 w-full" type="text" name="localidad" :value="old('localidad')" required />
                </div>
                <div class="mt-4 col">
                    <x-jet-label for="dni" value="Dni *" />
                    <x-jet-input id="dni" class="block mt-1 w-full" type="text" name="dni" :value="old('dni')" required />
                </div>
            </div>
            <div class="mt-4">
                <x-jet-label for="password" value="Contraseña *" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="Confirma tu contraseña *" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>
            
            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    Volver
                </a>
                <a class="underline text-sm text-gray-600 hover:text-gray-900 ml-20" href="{{ route('login') }}">
                    ¿Ya estás registrado?
                </a>

                <x-jet-button class="ml-10">
                    Registrar
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
