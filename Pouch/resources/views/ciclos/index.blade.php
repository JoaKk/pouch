@extends('adminlte::page')

@section('title', 'Ciclos')

@section('content')
@livewire('ciclo.ciclos-index')
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')

@stop
