@extends('adminlte::page')

@section('title', ' Detalle ciclo')

@section('content_header')
    <h1>Detalle de ciclo</h1>
@stop

@section('content')
    <div class="container py-8">
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
            <h4>Proyectos relacionados con este ciclo</h4>
            <ul>
                @foreach ($ciclo->proyectos as $proyecto)
                   <a href="{{route('proyectos.show',$proyecto)}}"><li>{{$proyecto->nombre}}</li></a>
                @endforeach
            </ul>
        </div>
        <div class="mt-4">
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')
    <script>
        console.log('Hi!');

    </script>
@stop
