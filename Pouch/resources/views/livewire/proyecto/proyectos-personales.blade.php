<div>
    <div class="card">
        <div class="card-header bg-success p-0 py-2">
            <section class="content-header">
                <h3 class="card-title text-bold"><i class="fas fa-fx fa-crown"></i> LISTA DE MIS PROYECTOS PERSONALES</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item active text-white-50">Mis proyectos</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="proyectosPersonales">
                <thead>
                    <tr>
                        <th style="width: 25%">
                            Nombre del proyecto
                        </th>
                        <th style="width: 20%">
                            Progreso del proyecto
                        </th>
                        <th style="width: 20%">
                            Ciclo Formativo
                        </th>
                        <th style="width: 3%" class="text-center">
                            Estado
                        </th>
                        <th>
                            Puntuación
                        </th>
                        <th style="width: 5%">
                        </th>
                        <th style="width: 5%">
                        </th>
                        <th style="width: 5%">
                        </th>
                        <th style="width: 5%">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($proyectos as $proyecto)
                        <tr>
                            <td>
                                <a href="{{ route('proyectos.show', $proyecto) }}">
                                    {{ $proyecto->nombre }}
                                </a>
                                <br />
                                <small>
                                    Publicado el {{ $proyecto->created_at->format('d-m-Y') }}
                                </small>
                            </td>
                            <td class="project_progress">
                                <div class="progress progress-sm">
                                    @php
                                        $random = rand(0, 100);
                                    @endphp
                                    @if ($random < 25)
                                        <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"
                                            role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0"
                                            aria-valuemax="100" style="width:{{ $random }}%">
                                        </div>

                                    @elseif ($random < 50) <div
                                            class="progress-bar bg-info progress-bar-striped progress-bar-animated"
                                            role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0"
                                            aria-valuemax="100" style="width:{{ $random }}%">
                                </div>

                            @elseif ($random < 75) <div
                                    class="progress-bar bg-warning progress-bar-striped progress-bar-animated"
                                    role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0"
                                    aria-valuemax="100" style="width:{{ $random }}%">
        </div>

    @elseif ($random <= 100) <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated"
            role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0" aria-valuemax="100"
            style="width:{{ $random }}%">
    </div>
    @endif
</div>
<small>
    {{ $random }}% Completado
</small>
</td>
<td>
    <i>{{ $proyecto->ciclo->nombre }}</i>
</td>
<td class="project-state">
    @if ($proyecto->publico)
        <span class="badge badge-success">Público</span>
    @else
        <span class="badge badge-danger">Privado</span>
    @endif
</td>
<td>
    @if (empty($proyecto->puntuacion))
        <p>Sin evaluar</p>
    @else
        <p>{{ $proyecto->puntuacion }} / 100</p>
    @endif
</td>
<td>
    <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ver Proyecto"
        href="{{ route('proyectos.show', $proyecto) }}">
        <i class="fas fa-eye m-auto">
        </i>
    </a>
</td>
<td>
    <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Editar Proyecto"
        href="{{ route('proyectos.edit', $proyecto) }}">
        <i class="fas fa-pencil-alt m-auto">
        </i>
    </a>
</td>
<td>
    @if (empty($proyecto->puntuacion))
        <a class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Asignar Profesores"
            href="{{ route('proyectos.asignarUsuarios', $proyecto) }}">
            <i class="fas fa-plus m-auto">
            </i>
        </a>
    @else
        <span class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Proyecto Evaluado">
            <i class="fas fa-check">
            </i></span>
    @endif
</td>
<td>
    <form class="formulario-eliminar" action="{{ route('proyectos.destroy', $proyecto) }}" method="POST">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Borrar Proyecto"
            type="submit"><i class="fas fa-trash m-auto">
            </i></button>
    </form>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>

</div>
