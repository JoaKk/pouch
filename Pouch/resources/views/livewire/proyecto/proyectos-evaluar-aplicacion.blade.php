<div>
    <div class="card">
        <div class="card-header bg-dark p-0 py-2">
            <section class="content-header">
                <h3 class="card-title"><span class="fas fa-fx fa-tasks"></span> EVALUACIÓN DE LA <b>APLICACIÓN</b> DEL PROYECTO:
                    <b>{{ $proyecto->nombre }}</b>
                </h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item"><a class="text-white"
                            href="{{ route('proyectos.evaluar.index') }}">Evaluar proyecto</a></li>
                    <li class="breadcrumb-item"><a class="text-white"
                            href="{{ route('proyecto.secciones', $proyecto) }}">Secciones</a></li>
                    <li class="breadcrumb-item active text-white-50">Aplicación</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            <div class="bg-success m-auto rounded row">
                <div class="col-8">
                    <h5 class="mt-2">Anotaciones para la evaluación del proyecto :</h5>
                    <ul class="font-medium">
                        <li>La puntuación será como mínimo de 1 hasta un máximo de 5.</li>
                        <li>La puntuación se guarda automáticamente.</li>
                        <li>Una vez completados todos los campos, podrá guardar la puntuación total.</li>
                    </ul>
                </div>
                <div class="col-4" style="margin:auto">
                    <button class="btn btn-danger float-right"
                        onclick="window.open('{{ route('proyectos.mostrarPDF', $proyecto) }}', '_blank');">ABRIR
                        DOCUMENTACIÓN DEL PROYECTO</button>
                </div>
            </div>
            <table class="table table-striped" id="rubrica">
                @foreach ($rubricasCab as $rubricaCab)
                    <tbody>
                        <th class="bg-dark">
                            {{ $rubricaCab->descripcion }} ({{ $rubricaCab->porcentaje }}%)
                        </th>
                        <th class="bg-info text-center" style="width: 8%">Nota</th>
                        @foreach ($rubricaCab->rubricasDet as $rubrica)
                            <tr>
                                <td>{{ $rubrica->descripcion }}
                                    @if (!$finalizado)
                                        @if (!empty($notas[$rubrica->id]))
                                            <button data-target="#myModal{{ $rubrica->id }}"
                                                id="btn.{{ $rubrica->id }}"
                                                class="bnt btn-danger rounded float-right">
                                                <i class="fas fa-pencil-alt"></i></button>
                                        @else
                                            <button data-target="#myModal{{ $rubrica->id }}"
                                                id="btn.{{ $rubrica->id }}"
                                                class="bnt btn-success rounded float-right">
                                                <i class="fas fa-pencil-alt"></i></button>
                                        @endif
                                    @endif
                                </td>
                                <td class="border">
                                    <input readonly wire:model="notas.{{ $rubrica->id }}"
                                        class="form-control text-center" id="input{{ $rubrica->id }}" type="number">
                                </td>
                                <div class="modal fade" id="myModal{{ $rubrica->id }}" tabindex="-1" role="dialog"
                                    aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                @if (!empty($notas[$rubrica->id]))
                                                    <h4 class="modal-title col-11 text-center text-bold"
                                                        id="myModalLabel">Editar Puntuación</h4>
                                                @else
                                                    <h4 class="modal-title col-11 text-center text-bold"
                                                        id="myModalLabel">Asignar Puntuación</h4>
                                                @endif
                                            </div>
                                            <div class="modal-body">
                                                <small class="text-bold">Rúbrica :</small>
                                                <small>{{ $rubrica->descripcion }}</small>
                                                <br>
                                                <hr>
                                                <small class="text-bold mt-3">Puntuación : </small>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio"
                                                        name="inlineRadioOptions{{ $rubrica->id }}"
                                                        wire:model.defer="notas.{{ $rubrica->id }}"
                                                        id="inlineRadio1{{ $rubrica->id }}" value="1">
                                                    <label class="form-check-label"
                                                        for="inlineRadio1{{ $rubrica->id }}">1</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio"
                                                        name="inlineRadioOptions{{ $rubrica->id }}"
                                                        wire:model.defer="notas.{{ $rubrica->id }}"
                                                        id="inlineRadio2{{ $rubrica->id }}" value="2">
                                                    <label class="form-check-label"
                                                        for="inlineRadio2{{ $rubrica->id }}">2</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio"
                                                        name="inlineRadioOptions{{ $rubrica->id }}"
                                                        wire:model.defer="notas.{{ $rubrica->id }}"
                                                        id="inlineRadio3{{ $rubrica->id }}" value="3">
                                                    <label class="form-check-label"
                                                        for="inlineRadio3{{ $rubrica->id }}">3</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio"
                                                        name="inlineRadioOptions{{ $rubrica->id }}"
                                                        wire:model.defer="notas.{{ $rubrica->id }}"
                                                        id="inlineRadio4{{ $rubrica->id }}" value="4">
                                                    <label class="form-check-label"
                                                        for="inlineRadio4{{ $rubrica->id }}">4</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio"
                                                        name="inlineRadioOptions{{ $rubrica->id }}"
                                                        wire:model.defer="notas.{{ $rubrica->id }}"
                                                        id="inlineRadio5{{ $rubrica->id }}" value="5">
                                                    <label class="form-check-label"
                                                        for="inlineRadio5{{ $rubrica->id }}">5</label>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                @if (!empty($notas[$rubrica->id]))
                                                    <button type="button" class="btn btn-default align-center"
                                                        data-dismiss="modal">Cerrar</button>
                                                    <button wire:click="almacenarNotas({{ $rubrica->id }})"
                                                        data-keyboard="true"
                                                        class="btn btn-danger action">Editar</button>
                                                @else
                                                    <button type="button" class="btn btn-default align-center"
                                                        data-dismiss="modal"
                                                        id="closeBtn{{ $rubrica->id }}">Cerrar</button>
                                                    <button disabled wire:click="almacenarNotas({{ $rubrica->id }})"
                                                        data-keyboard="true" class="btn btn-primary"
                                                        id="btn{{ $rubrica->id }}">Guardar</button>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        @endforeach
                    </tbody>
                @endforeach
            </table>
            @if (!$finalizado)
                <button @if (!$relleno) echo disabled @endif
                    wire:click="finalizarPuntuacion" class="btn btn-primary mt-2 float-right">Guardar
                    Evaluación</button>
            @endif
        </div>
    </div>
</div>
</div>
