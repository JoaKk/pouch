<div>
    <div class="card">
        <div class="card-header bg-dark p-0 py-2">
            <section class="content-header">
                <h3 class="card-title text-bold"><span class="fas fa-fx fa-file-alt"></span> LISTA DE PROYECTOS ASIGNADOS</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item active text-white-50">Evaluar proyecto</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="users">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Autor</th>
                        <th>Ciclo Formativo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($proyectos as $proyecto)
                        <tr>
                            <td>{{$proyecto->nombre}}</td>
                            <td>{{$proyecto->user->name}}</td>
                            <td>{{$proyecto->ciclo->nombre}}</td>
                            <td>
                                <a class="btn btn-primary" href="{{route('proyecto.secciones',$proyecto)}}">Evaluar proyecto</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
