<div>
    <div class="card">
        <div class="card-header bg-success p-0 py-2">
            <section class="content-header">
                <h3 class="card-title text-bold"><i class="fas fa-fx fa-list"></i> LISTA DE PROYECTOS DE OTROS ALUMNOS</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item active text-white-50">Lista de proyectos</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="proyectos">
                <thead>
                    <tr>
                        <th style="width: 8%">
                            Autor
                        </th>
                        <th style="width: 25%">
                            Nombre del proyecto
                        </th>
                        <th style="width: 20%">
                            Ciclo Formativo
                        </th>
                        <th style="width: 20%">
                            Progreso del proyecto
                        </th>
                        <th style="width: 8%">
                            Descargas
                        </th>
                        <th style="width: 8%" class="text-center">
                            Estado
                        </th>
                        <th style="width: 18%">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($proyectos as $proyecto)
                        <tr>
                            <td>
                                <ul class="list-inline mt-2 mb-0">
                                    <li class="list-inline-item">
                                        <div class="container-overlay">
                                            <a href="{{ route('perfil', $proyecto->user) }}">
                                                <img alt="Avatar" id="avatar" title="{{ $proyecto->user->name }}"
                                                    class="table-avatar img-circle"
                                                    src="{{ $proyecto->user->adminlte_image() }}">
                                                <div class="overlay table-avatar img-circle"
                                                    style="background-color:rgba(26, 19, 19, 0.76)">
                                                    {{ $proyecto->user->name }}</div>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <a class="text-bold" href="{{ route('proyectos.show', $proyecto) }}">
                                    {{ $proyecto->nombre }}
                                </a>
                                <br />
                                @if ($proyecto->created_at == $proyecto->updated_at)
                                    <small>
                                        Publicado hace {{ $proyecto->calcularTiempo($proyecto->created_at) }}
                                    </small>
                                @else
                                    <small>
                                        Actualizado hace {{ $proyecto->calcularTiempo($proyecto->updated_at) }}
                                    </small>
                                @endif
                            </td>
                            <td>
                                <i>{{ $proyecto->ciclo->nombre }}</i>
                            </td>
                            <td class="project_progress">
                                <div class="progress progress-sm">
                                    @php
                                        $random = rand(0, 100);
                                    @endphp
                                    @if ($random < 25)
                                        <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"
                                            role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0"
                                            aria-valuemax="100" style="width:{{ $random }}%">
                                        </div>

                                    @elseif ($random < 50) <div
                                            class="progress-bar bg-info progress-bar-striped progress-bar-animated"
                                            role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0"
                                            aria-valuemax="100" style="width:{{ $random }}%">
                                </div>

                            @elseif ($random < 75) <div
                                    class="progress-bar bg-warning progress-bar-striped progress-bar-animated"
                                    role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0"
                                    aria-valuemax="100" style="width:{{ $random }}%">
        </div>

    @elseif ($random <= 100) <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated"
            role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0" aria-valuemax="100"
            style="width:{{ $random }}%">
    </div>
    @endif
</div>
<small>
    {{ $random }}% Completado
</small>
</td>
<td>
    <i class="fas fa-fx fa-cloud-download-alt"> </i> {{ $proyecto->descargas }}
</td>
<td class="project-state">
    @if ($proyecto->publico)
        <span class="badge badge-success">Público</span>
    @else
        <span class="badge badge-danger">Privado</span>
    @endif
</td>
<td class="project-actions text-right">
    <a class="btn btn-primary btn-sm" href="{{ route('proyectos.show', $proyecto) }}">
        Ver Proyecto
    </a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>

</div>
