<div>
    <div class="card">
        <div class="card-header bg-info p-0 py-2">
            <section class="content-header">
                <h3 class="card-title text-bold"><span class="fab fa-fx fa-buffer"></span> LISTA DE CICLOS FORMATIVOS</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item active text-white-50">Ciclos</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="users">
                <thead>
                    <tr class="bg-secondary">
                        <th class="text-center border-right" style="width: 10%">Familia</th>
                        <th style="width: 50%" >Ciclo Formativo Grado Superior</th>
                        <th class="border-right" style="width: 25%"></th>
                        <th class="text-right" style="width: 15%"></th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $seccion = '';
                    @endphp
                    @foreach ($ciclosSecciones as $ciclo)
                        @php
                            $ciclosPorSeccion = $ciclo->ciclosPorSeccion($ciclo->seccion);
                        @endphp

                        @foreach ($ciclosPorSeccion as $cicloAux)
                            <tr>
                                @if ($cicloAux->seccion != $seccion)
                                    <td class="bg-info text-bold" rowspan="{{ count($ciclosPorSeccion) }}"
                                        align="center" style="vertical-align: middle">
                                        {{ $cicloAux->seccion }}</td>
                                @endif
                                <td style="vertical-align: middle"><a href="{{ $cicloAux->link }}"
                                        target="_blank">{{ $cicloAux->nombre }}</a></td>
                                <td style="vertical-align: middle"></td>
                                @if ($cicloAux->seccion != $seccion)
                                    <td class="bg-info" rowspan="{{ count($ciclosPorSeccion) }}" align="center"
                                        style="vertical-align: middle"><button class="btn btn-warning"><b>Ver Proyectos
                                                Relacionados</b></button></td>
                                @endif
                            </tr>
                            @php
                                $seccion = $cicloAux->seccion;
                            @endphp
                        @endforeach

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
