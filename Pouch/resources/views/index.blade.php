@extends('adminlte::page')

@section('title', 'Inicio')

@section('content')
    <div class="row">
        <div class="col">
            <!-- small box -->
            <div class="small-box bg-primary">
                <div class="inner">
                    <h3>COMPARTE</h3>
                    <p style="width: 80%">Almacena en nuestro repositorio tu proyecto final de ciclo formativo.</p>
                </div>
                <div class="icon">
                    <i class="fas fa-arrow-circle-up"></i>
                </div>
                <a href="{{ route('proyectos.create') }}" class="small-box-footer"> ENTRAR <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>EXPLORA</h3>

                    <p>Busca proyectos creados por otros alumnos y descarga sus recursos.</p>
                </div>
                <div class="icon">
                    <i class="fas fa-search"></i>
                </div>
                <a href="{{ route('proyectos.index') }}" class="small-box-footer"> ENTRAR <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="w-100"></div>
        <div class="col">
            <!-- small box -->
            <div class="small-box bg-secondary">
                <div class="inner">
                    <h3>EVALÚA</h3>

                    <p>Evalúa los proyectos de tus alumnos mediante rúbricas personalizadas.</p>
                </div>
                <div class="icon">
                    <i class="fas fa-tasks"></i>
                </div>
                @if (auth()->user()->adminlte_desc() == "Profesor" || auth()->user()->adminlte_desc() == "Administrador")
                <a href="{{ route('proyectos.evaluar.index') }}" class="small-box-footer"> ENTRAR <i class="fas fa-arrow-circle-right"></i></a>
                @elseif(auth()->user()->adminlte_desc() == "Alumno")
                <a role="button" disabled class="small-box-footer"><i class="fas fa-arrow-circle-riwqeght"></i></a>
                @endif
                
            </div>
        </div>
        <!-- ./col -->
        <div class="col">
            <!-- small box -->

            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>CICLOS FORMATIVOS</h3>

                    <p>Revisa los distintos ciclos formativos de grado superior de Cantabria.</p>
                </div>
                <div class="icon">
                    <i class="fas fa-list-alt"></i>
                </div>
                <a href="{{ route('ciclos.index') }}" class="small-box-footer"> ENTRAR <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="w-100"></div>
        <div class="col">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="ribbon-wrapper ribbon-xl">
                    <div class="ribbon bg-danger text-lg">
                        En desarrollo
                    </div>
                </div>
                <div class="inner">
                    <h3>GESTIONA</h3>

                    <p>Organiza las tareas de tu proyecto mediante el método <i>Kanban</i>.</p>
                </div>
                <div class="icon">
                    <i class="fas fa-calendar-alt"></i>
                </div>
                <a class="small-box-footer"><i class="fas fa-arrow-circle-rigASht"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="ribbon-wrapper ribbon-xl">
                    <div class="ribbon bg-warning text-lg">
                        En desarrollo
                    </div>
                </div>
                <div class="inner">
                    <h3>EJECUTA</h3>

                    <p>Despliega los proyectos mediante los contenedores de <i>Docker</i>.</p>
                </div>
                <div class="icon">
                    <i class="fas fa-cog"></i>
                </div>
                <a  class="small-box-footer"> <i class="fas fa-arrow-circle-rigWERWRht"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop
