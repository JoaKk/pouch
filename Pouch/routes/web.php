<?php

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\CicloController;
use App\Http\Controllers\PerfilController;
use App\Http\Controllers\ProfesorController;
use App\Http\Controllers\ProyectoController;
use Illuminate\Support\Facades\Route;
use App\Models\Proyecto;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (auth()->user()) {
        return redirect()->route('inicio');
    }
    return view('welcome');
})->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('inicio',[HomeController::class,'index'])->name('inicio');

Route::resource('admin/users',UserController::class)->only(['index','edit','update'])->middleware(['auth:sanctum', 'verified'])->names('admin.users');

Route::middleware(['auth:sanctum', 'verified'])->get('ciclos', [CicloController::class, 'index'])->name('ciclos.index');

Route::middleware(['auth:sanctum', 'verified'])->get('ciclos/{ciclo}', [CicloController::class, 'show'])->name('ciclos.show');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/mis-proyectos', [ProyectoController::class, 'personales'])->name('proyectos.personales');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/mis-proyectos/{proyecto}/asignarUsuarios', [ProyectoController::class, 'asignarUsuarios'])->name('proyectos.asignarUsuarios');

Route::middleware(['auth:sanctum', 'verified'])->post('proyectos/mis-proyectos/{proyecto}/asignarUsuarios', [ProyectoController::class, 'asignar'])->name('proyectos.asignar');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/mis-proyectos/{proyecto}/desasignar/{user}', [ProyectoController::class, 'desasignar'])->name('proyectos.desasignar');

Route::middleware(['auth:sanctum', 'verified'])->post('proyectos/{proyecto}/comentar', [ProyectoController::class, 'comentar'])->name('proyectos.comentar');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/{proyecto}/eliminarComentario', [ProyectoController::class, 'eliminarComentario'])->name('proyectos.eliminarComentario');

Route::middleware(['auth:sanctum', 'verified','can:proyectos.evaluar.index'])->get('proyectos/evaluar', [ProyectoController::class, 'evaluarIndex'])->name('proyectos.evaluar.index');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/evaluar/{proyecto}', [ProyectoController::class, 'evaluarSecciones'])->name('proyecto.secciones');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/evaluar/{proyecto}/exposicion',[ProyectoController::class,'evaluarExposicion'])->name('proyecto.evaluarExposicion');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/evaluar/{proyecto}/documentacion',[ProyectoController::class,'evaluarDocumentacion'])->name('proyecto.evaluarDocumentacion');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/evaluar/{proyecto}/aplicacion',[ProyectoController::class,'evaluarAplicacion'])->name('proyecto.evaluarAplicacion');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/evaluar/{proyecto}/seguimiento',[ProyectoController::class,'evaluarSeguimiento'])->name('proyecto.evaluarSeguimiento');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/evaluar/{proyecto}/registrarPuntuacion',[ProyectoController::class,'registrarPuntuacion'])->name('proyecto.registrarPuntuacion');

Route::resource('proyectos', ProyectoController::class)->names('proyectos')->middleware(['auth:sanctum', 'verified']);

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/{proyecto}/descargar', [ProyectoController::class, 'download'])->name('proyectos.download');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/{proyecto}/descargarLogo', [ProyectoController::class, 'descargarLogo'])->name('proyectos.descargarLogo');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/{proyecto}/descargarRecurso/{recurso}', [ProyectoController::class, 'descargarRecurso'])->name('proyectos.descargarRecurso');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/{proyecto}/mostrarPDF', [ProyectoController::class, 'mostrarPDF'])->name('proyectos.mostrarPDF');

Route::middleware(['auth:sanctum', 'verified'])->get('perfil/{user}', [PerfilController::class, 'index'])->name('perfil');

Route::middleware(['auth:sanctum', 'verified'])->get('profesores/rubricas',[ProfesorController::class,'index'])->name('profesores');

/*Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');*/
