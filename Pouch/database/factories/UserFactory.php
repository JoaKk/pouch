<?php

namespace Database\Factories;

use App\Models\Instituto;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $randomColor = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
        $nombre = $this->faker->name;
        return [
            'name' => $nombre,
            'slug' => Str::slug($nombre),
            'dni' => $this->faker->dni,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'publico' => $this->faker->boolean,
            'localidad' => $this->faker->city,
            'instituto_id' => Instituto::all()->random()->id,
            'profile_photo_path' => 'https://ui-avatars.com/api/?name='.urlencode($nombre).'&color=FFFFFF&background='.$randomColor.'&bold=true'
        ];
    }
}
