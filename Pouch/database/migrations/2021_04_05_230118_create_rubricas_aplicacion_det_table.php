<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRubricasAplicacionDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rubricas_aplicacion_det', function (Blueprint $table) {
            $table->id();
            $table->longText('descripcion');
            $table->unsignedBigInteger('rubrica_aplicacion_cab_id');
            $table->foreign('rubrica_aplicacion_cab_id')->references('id')->on('rubricas_aplicacion_cab')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rubricas_aplicacion_det');
    }
}
