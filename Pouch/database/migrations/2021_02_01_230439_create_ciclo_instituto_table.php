<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCicloInstitutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ciclo_instituto', function (Blueprint $table) {
            $table->unsignedBigInteger('ciclo_id');
            $table->unsignedBigInteger('instituto_id');
            $table->foreign('ciclo_id')->references('id')->on('ciclos');
            $table->foreign('instituto_id')->references('id')->on('institutos');
            $table->primary(['ciclo_id','instituto_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ciclo_instituto');
    }
}
