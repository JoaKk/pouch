<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRubricasSeguimientoDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rubricas_seguimiento_det', function (Blueprint $table) {
            $table->id();
            $table->longText('descripcion');
            $table->unsignedBigInteger('rubrica_seguimiento_cab_id');
            $table->foreign('rubrica_seguimiento_cab_id')->references('id')->on('rubricas_seguimiento_cab')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rubricas_seguimiento_det');
    }
}
