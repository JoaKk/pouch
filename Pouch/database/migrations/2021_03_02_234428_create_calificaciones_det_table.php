<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalificacionesDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calificaciones_det', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('proyecto_id');
            $table->foreign('proyecto_id')->references('id')->on('proyectos')->onDelete('cascade');
            $table->unsignedBigInteger('calificacion_cab_id');
            $table->foreign('calificacion_cab_id')->references('id')->on('calificaciones_cab')->onDelete('cascade');
            $table->unsignedBigInteger('rubrica_cab_id');
            $table->foreign('rubrica_cab_id')->references('id')->on('rubricas_documentacion_cab')->onDelete('cascade');
            $table->unsignedBigInteger('rubrica_det_id');
            $table->foreign('rubrica_det_id')->references('id')->on('rubricas_documentacion_det')->onDelete('cascade');
            $table->float('puntuacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calificaciones_det');
    }
}
