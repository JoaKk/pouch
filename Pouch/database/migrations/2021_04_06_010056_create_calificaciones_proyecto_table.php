<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalificacionesProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calificaciones_proyecto', function (Blueprint $table) {
            $table->unsignedBigInteger('calificacion_cab_id');
            $table->unsignedBigInteger('proyecto_id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('calificacion_cab_id','calificaciones_cab')->references('id')->on('calificaciones_cab')->onDelete('cascade');
            $table->foreign('proyecto_id','proyecto')->references('id')->on('proyectos')->onDelete('cascade');
            $table->foreign('user_id','user')->references('id')->on('users')->onDelete('cascade');
            $table->primary(['calificacion_cab_id','proyecto_id','user_id'],'id');
            $table->float('puntuacion');
            $table->boolean('finalizado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calificaciones_proyecto');
    }
}
