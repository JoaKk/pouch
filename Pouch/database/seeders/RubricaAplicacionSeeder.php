<?php

namespace Database\Seeders;

use App\Models\RubricaAplicacionCab;
use App\Models\RubricaAplicacionDet;
use Illuminate\Database\Seeder;

class RubricaAplicacionSeeder extends Seeder
{
    private $datos = array(
        array(
            "cabecera" => "Puesta en marcha. Explotación",
            "rubricas" => array(
                "Se realiza un informe con los cambios necesarios para llevar a cabo la puesta en marcha y llevarlo a la explotación."
            ),
            "porcentaje" => 50,
        ),
        array(
            "cabecera" => "Pruebas y Calidad",
            "rubricas" => array(
                "Se realizan los casos de prueba como mínimo relacionados con los requisítos funcionales del proyecto.",
                "Se establece un plan de evaluación por parte del cliente antes, durante y después de su implementación, estableciendo indicadores de calidad."
            ),
            "porcentaje" => 50,
        ),
    );
    public function run()
    {
        foreach ($this->datos as $dato) {
            $rubricaCab = RubricaAplicacionCab::create([
                'descripcion' => $dato["cabecera"],
                'porcentaje' => $dato["porcentaje"]
            ]);
            foreach ($dato["rubricas"] as $rubrica) {
                RubricaAplicacionDet::create([
                    'descripcion' => $rubrica,
                    'rubrica_aplicacion_cab_id' => $rubricaCab->id,
                ]);
            }
        }
    }
}
