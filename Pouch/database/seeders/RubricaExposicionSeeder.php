<?php

namespace Database\Seeders;

use App\Models\RubricaExposicionCab;
use App\Models\RubricaExposicionDet;
use Illuminate\Database\Seeder;

class RubricaExposicionSeeder extends Seeder
{
    private $datos = array(
        array(
            "cabecera" => "Exposicion",
            "rubricas" => array(
                "Se han presentado todos los puntos de la memoria con claridad ,orden y fluidez.",
                "Se ha utilizado alguna herramienta auxiliar de apoyo para la exposición (ppt, prezi,video, ….).",
                "Se ha mostrado el funcionamiento técnico del proyecto piloto (maqueta, video, prototipo, …).",
                "Se ha respondido con solvencia las cuestiones planteadas por el equipo educativo."
            ),
            "porcentaje" => 100,
        )
    );
    public function run()
    {
        foreach ($this->datos as $dato) {
            $rubricaCab = RubricaExposicionCab::create([
                'descripcion' => $dato["cabecera"],
                'porcentaje' => $dato["porcentaje"]
            ]);
            foreach ($dato["rubricas"] as $rubrica) {
                RubricaExposicionDet::create([
                    'descripcion' => $rubrica,
                    'rubrica_exposicion_cab_id' => $rubricaCab->id,
                ]);
            }
        }
    }
}
