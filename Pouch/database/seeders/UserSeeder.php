<?php

namespace Database\Seeders;

use App\Models\Instituto;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin Repo',
            'slug' => 'Admin-Repo',
            'dni' => '72151524H',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'publico' => 1,
            'localidad' => 'Cantabria',
            'instituto_id' => Instituto::all()->random()->id,
            'profile_photo_path' => 'https://picsum.photos/200'
        ])->assignRole('Administrador','Profesor');
        $users = User::factory(30)->create();
        foreach ($users as $user) {
            $user->assignRole('Alumno');
        }
        $users = User::factory(10)->create();
        foreach ($users as $user) {
            $user->assignRole('Profesor');
        }
    }
}
