<?php

namespace Database\Seeders;

use App\Models\Ciclo;
use App\Models\Comentario;
use App\Models\ControlUser;
use App\Models\Proyecto;
use App\Models\Recurso;
use App\Models\TipoRecurso;
use App\Models\User;
use App\Models\Version;
use App\Traits\ControlUsersTrait;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ProyectoSeeder extends Seeder
{

    public function run()
    {
        $proyectos = Proyecto::factory(40)->create();
        foreach ($proyectos as $proyecto) {
            $proyecto->update([
                'ruta_logo' => Storage::putFileAs('recursos/' . $proyecto->id, public_path('images/defaultwhite.jpg'), 'defaultwhite.jpg')
            ]);
            ControlUser::create([
                'user_id' => $proyecto->user_id,
                'proyecto_id' => $proyecto->id,
                'accion' => "PROYECTO CREADO"
            ]);
            Storage::makeDirectory('recursos/' . $proyecto->id);
            Recurso::create([
                'proyecto_id' => $proyecto->id,
                'ruta_fichero' => Storage::putFileAs('recursos/' . $proyecto->id, public_path('images/default.rar'), 'default.rar'),
                'tipo_id' => 1
            ]);
            Recurso::create([
                'proyecto_id' => $proyecto->id,
                'ruta_fichero' => Storage::putFileAs('recursos/' . $proyecto->id, public_path('images/default.pdf'), 'default.pdf'),
                'tipo_id' => 3
            ]);
            Version::factory(2)->create([
                'proyecto_id' => $proyecto->id
            ]);
            Comentario::factory(1)->create([
                'proyecto_id' => $proyecto->id,
                'user_id' => User::all()->random()->id
            ]);
            Comentario::factory(1)->create([
                'proyecto_id' => $proyecto->id,
                'user_id' => User::all()->random()->id
            ]);
            Comentario::factory(1)->create([
                'proyecto_id' => $proyecto->id,
                'user_id' => User::all()->random()->id
            ]);
        }
    }
}
