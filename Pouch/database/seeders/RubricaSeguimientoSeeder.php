<?php

namespace Database\Seeders;

use App\Models\RubricaSeguimientoCab;
use App\Models\RubricaSeguimientoDet;
use Illuminate\Database\Seeder;

class RubricaSeguimientoSeeder extends Seeder
{
    private $datos = array(
        array(
            "cabecera" => "Seguimiento",
            "rubricas" => array(
                "Entrega de tareas en fecha y tiempo siguiendo las directrices de los tutores.",
                "Se realizan las correcciones necesarias siguiendo las directrices del tutor en tiempo y fecha.",
                "Seguimiento de las instrucciones de entrega en la plataforma y con las directrices dadas por el tutor.",
                "Se rellena y utiliza la hoja de seguimiento semanal."
            ),
            "porcentaje" => 100,
        )
    );
    public function run()
    {
        foreach ($this->datos as $dato) {
            $rubricaCab = RubricaSeguimientoCab::create([
                'descripcion' => $dato["cabecera"],
                'porcentaje' => $dato["porcentaje"]
            ]);
            foreach ($dato["rubricas"] as $rubrica) {
                RubricaSeguimientoDet::create([
                    'descripcion' => $rubrica,
                    'rubrica_seguimiento_cab_id' => $rubricaCab->id,
                ]);
            }
        }
    }
}
