<?php

namespace Database\Seeders;

use App\Models\RubricaDocumentacionCab;
use App\Models\RubricaDocumentacionDet;
use Illuminate\Database\Seeder;

class RubricaDocumentacionSeeder extends Seeder
{
    private $datos = array(
        array(
            "cabecera" => "Introducción",
            "rubricas" => array(
                "Se explica claramente que se va a desarrollar en su proyecto. El porqué de la elección."
            ),
            "porcentaje" => 10,
        ),
        array(
            "cabecera" => "Objetivo",
            "rubricas" => array(
                "Se presentan los objetivos enumerados generales y específicos del proyecto que se pretenden conseguir."
            ),
            "porcentaje" => 10,
        ),
        array(
            "cabecera" => "Análisis de Contexto y Estado del Arte",
            "rubricas" => array(
                "Se clasifican las empresas del sector por sus características y el tipo de producto.",
                "Se han valorado las oportunidades de negocio del sector.",
                "Se realiza un estudio de las mejoras que tú puedes aportar con tu proyecto, ventajas, inconvenientes."
            ),
            "porcentaje" => 10,
        ),
        array(
            "cabecera" => "Análisis de Requisítos",
            "rubricas" => array(
                "Se desarrollan los requisítos no funcionales de la aplicación.",
                "Se enumeran los requisítos funcionales de la aplicación.",
                "Se relacionan los requisítos con los objetivos del proyecto."
            ),
            "porcentaje" => 10,
        ),
        array(
            "cabecera" => "Diseño",
            "rubricas" => array(
                "Se desarrolla y explica un esquema general de la arquitectura del proyecto.",
                "Se desarrolla y explica un esquema E-R si se trabajan con BD.",
                "Se desarrolla y explica un diagrama de clases.",
                "Se desarrolla y explica un diagrama de interfaces con mapa de navegación",
                "Se desarrolla y explica un diagrama de procedimientos"
            ),
            "porcentaje" => 15,
        ),
        array(
            "cabecera" => "Planificación",
            "rubricas" => array(
                "Se han previsto los recursos materiales y personales necesarios para realizar el proyecto.",
                "Se han secuenciado las actividades, ordenándolas en función de las necesidades de implementación (Gantt).",
                "Se han planificado la asignación de recursos materiales y humanos y los tiempos de ejecución."
            ),
            "porcentaje" => 10,
        ),
        array(
            "cabecera" => "Implementación",
            "rubricas" => array(
                "Se realiza un prototipo de la aplicación.",
                "Se desarrollan algunos de los aspectos no tan básicos del proyecto.",
                "Se documenta la implementación explicando en la memoria aspectos relevantes del código desarrollado."
            ),
            "porcentaje" => 15,
        ),
        array(
            "cabecera" => "Plan de Empresa",
            "rubricas" => array(
                "Se han caracterizado las empresas tipo indicando la estructura organizativa y las funciones de cada departamento.",
                "Se han determinado las obligaciones fiscales, laborales y de prevención de riesgos y sus condiciones de aplicación.",
                "Se han identificado posibles ayudas o subvenciones para la incorporación de nuevas tecnologías de producción o de servicio que se proponen."
            ),
            "porcentaje" => 10,
        ),
        array(
            "cabecera" => "Miscelánea",
            "rubricas" => array(
                "Se termina el proyecto con conclusiones y valoración personal.",
                "Se determinan posibles mejoras y mantenimiento del proyecto.",
                "Se elabora un manual de usuario u otros manuales necesarios.",
                "Se incorpora bibliografía, índice de ilustraciones y de contenidos",
                "Se valorará la presentación general del documento, faltas de otrografía, fluidez en la expresión escrita, formato..."
            ),
            "porcentaje" => 10
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->datos as $dato) {
            $rubricaCab = RubricaDocumentacionCab::create([
                'descripcion' => $dato["cabecera"],
                'porcentaje' => $dato["porcentaje"]
            ]);
            foreach ($dato["rubricas"] as $rubrica) {
                RubricaDocumentacionDet::create([
                    'descripcion' => $rubrica,
                    'rubrica_documentacion_cab_id' => $rubricaCab->id,
                ]);
            }
        }
    }
}
