<?php

namespace Database\Seeders;

use App\Models\Ciclo;
use App\Models\Instituto;
use App\Models\Rol;
use App\Models\RubricaDocumentacionDet;
use App\Models\TipoRecurso;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Storage::deleteDirectory('recursos');
        Storage::makeDirectory('recursos');
        $this->call(RoleSeeder::class);
        $this->call(TipoRecursoSeeder::class);
        Instituto::factory(10)->create();
        $this->call(CicloSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ProyectoSeeder::class);
        $this->call(RubricaDocumentacionSeeder::class);
        $this->call(RubricaExposicionSeeder::class);
        $this->call(RubricaAplicacionSeeder::class);
        $this->call(RubricaSeguimientoSeeder::class);
        $this->call(CalificacionCabSeeder::class);
    }
}
