<?php

namespace Database\Seeders;

use App\Models\CalificacionCab;
use Illuminate\Database\Seeder;

class CalificacionCabSeeder extends Seeder
{
    private $datos = array(
        array(
            "descripcion" => "Exposición",
            "porcentaje" => 15
        ),
        array(
            "descripcion" => "Documentación",
            "porcentaje" => 40
        ),
        array(
            "descripcion" => "Aplicación",
            "porcentaje" => 30
        ),
        array(
            "descripcion" => "Seguimiento",
            "porcentaje" => 15
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->datos as $dato){
            CalificacionCab::create([
                'descripcion' => $dato["descripcion"],
                'porcentaje' => $dato['porcentaje']
            ]);
        }
    }
}
