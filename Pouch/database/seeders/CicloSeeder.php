<?php

namespace Database\Seeders;

use App\Models\Ciclo;
use App\Models\Instituto;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CiclosImport;

class CicloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        Excel::import(new CiclosImport, public_path('images/CiclosFormativos.xlsx'));
    }
}
