//Stepper
const step1 = $(".div-step1");
const step2 = $(".div-step2");
const step3 = $(".div-step3");
const btnNext = $("#btn-next");
const btnNext1 = $("#btn-next1");
const btnPrev1 = $("#btn-prev1");
const btnNext2 = $("#btn-next2");
const btnPrev2 = $("#btn-prev2");


var stepper = new Stepper(document.querySelector("#stepper"), {
    animation: true
});

btnNext.click(function(e) {
    e.preventDefault();
    step2.removeClass("hidden");
    step1.addClass("hidden");
    stepper.next();
});
btnPrev1.click(function(e) {
    e.preventDefault();
    step1.removeClass("hidden");
    step2.addClass("hidden");
    stepper.previous();
});
btnNext1.click(function(e) {
    e.preventDefault();
    step3.removeClass("hidden");
    step2.addClass("hidden");
    stepper.next();
});
btnPrev2.click(function(e) {
    e.preventDefault();
    step2.removeClass("hidden");
    step3.addClass("hidden");
    stepper.previous();
});
btnNext2.click(function(e) {
    e.preventDefault();
    step4.removeClass("hidden");
    step3.addClass("hidden");
    stepper.next();
});


